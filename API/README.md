# Projet Todo List : API

Étape 1 : Ajouter les node_modules avec npm install

Étape 2 : Créer un fichier .env à la racine du dossier API avec les données suivantes à l'intérieur :

DATABASE_HOST=
DATABASE_USERNAME=postgres
DATABASE_NAME=ProjetTodoList
DATABASE_PASSWORD=(mot de passe à mettre si votre base de données PostGreSQL en a un)
DATABASE_PORT=5432
NODE_PORT=3000
NODE_END=dev

Étape 3 : Créer une base de données ProjetTodoList dans PostGreSQL.
		/ ! \ Ne surtout pas créer de tables ! Sequelize se chargera de les créer automatiquement à l'aide des Models.
		
Étape 4 : Lancer l'API avec node index.js