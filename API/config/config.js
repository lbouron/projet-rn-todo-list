require('dotenv').config();

const databaseHost = process.env.DATABASE_HOST;
const databaseUser = process.env.DATABASE_USERNAME;
const databaseName = process.env.DATABASE_NAME;
const databasePassword = process.env.DATABASE_PASSWORD;
const databasePort = process.env.DATABASE_PORT || '5432';

const port = process.env.PORT || '3000';
const nodeEnv = process.env.NODE_ENV;
const backendUrl = process.env.BACKEND_URL;

let config = module.exports;

config.api = {
    port: port,
    nodeEnv: nodeEnv,
	backendUrl: backendUrl || 'http://localhost:4001'
}

config.db = {
    user: databaseUser,
    password: databasePassword,
    name: databaseName
}

config.db.details = {
    host: databaseHost,
    port: databasePort,
    dialect: 'postgres',
    timezone: '+01:00',
    dialectOptions: {
        encrypt: true
    },
    logging: false
}

config.userRoles = {
    user: 'user'
};

config.keys = {
    secret: 'Qg,E-M0?GE+IW2k|a58?ZFo!A#X/demvkNtA}4>K1+V<,G/Sa~y9t(1c4f:#)<.'
}