const models = require('../models');
// const services = require('../services');
const Op = require('sequelize').Op;

let Controller = {
	signin: function (req, rep) {
		rep.json({ result: true, error: '' });
	},

	logout: function (req, rep) {
		req.logout();
		rep.status(200).json({ result: true, error: '' });
	},

	userInfo: async function (req, rep) {
		// let user = await models.users.findOne({ where: { id: req.users.id } });
		
		rep.status(200).json({
			id: req.user.id,
			email: req.user.email
		});
	},

	register: async function (req, rep) {
		let { email, password } = req.body;

		if (!email || !password) {
			rep.status(400).json({ result: false, error: 'Veuillez renseigner les champs obligatoires.' });
		} else {
			try {
				const exists = await models.users.count({ where: { email: email, password: { [Op.ne]: null } } });
				if (!exists) {
					let newUser = {
						email: email,
						password: password
					};

					await models.users.create(newUser);
					rep.status(201).json({ result: true, error: '' });
				} else {
					rep.status(409).json({ result: true, error: 'L\'email est déjà utilisé.' });
				}
			} catch (error) {
				rep.status(500).json({ result: false, error: error.message });
			}
		}
	}
}

module.exports = Controller;