const models = require('../models');
const logger = require('../services/logger');

let Controller = {
    getAll: async function (req, rep) {
        try {
            let results = await models.columns.findAll({
                order: [['title', 'ASC']],
                attributes: {
                    exclude: ['deletedAt']
                }
            });

            rep.status(200).json({ result: results, error: '' });
        } catch (error) {
            logger.error(error);
            rep.status(500).json({ result: false, error: 'Erreur interne' });
        }
    },

    add: async function (req, rep) {
        try {
            let { title, dashboardId } = req.body;

            if (!title) {
                rep.status(400).json({ result: false, error: 'La colonne doit avoir un titre.' });
            } else {
                let newColumn = {
                    title: title,
                    dashboardId: dashboardId
                };

                await models.columns.create(newColumn);
                rep.status(200).json({ result: true, error: '' });
            }
        } catch (error) {
            logger.error(error);
            rep.status(500).json({ result: false, error: 'Erreur interne' });
        }
    },

    updateById: async function (req, rep) {
        try {
            let data = await models.columns.findOne({ where: { id: req.params.id } });

            if (data) {
                await data.update(req.body);
                rep.status(200).json({ result: true, error: '' });
            } else {
                rep.status(404).json({ result: false, error: 'Aucun résultat pour cet identifiant.' });
            }

            await data.save();
        } catch (error) {
            logger.error(error);
            rep.status(500).json({ result: false, error: 'Erreur interne' });
        }
    },

    deleteById: async function (req, rep) {
        try {
            let data = await models.columns.findOne({ where: { id: req.params.id } });

            if (data) {
                await models.columns.destroy({ where: { id: req.params.id } });
                rep.status(200).json({ result: true, error: '' });
            } else {
                rep.status(404).json({ result: false, error: 'Aucun résultat pour cet identifiant.' });
            }
        } catch (error) {
            logger.error(error);
            rep.status(500).json({ result: false, error: 'Erreur interne' });
        }
    }
}

module.exports = Controller;