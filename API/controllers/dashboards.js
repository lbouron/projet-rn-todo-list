const models = require('../models');
const logger = require('../services/logger');

let Controller = {
    getAll: async function (req, rep) {
        try {
            let results = await models.dashboards.findAll({
                order: [['title', 'ASC']],
                attributes: {
                    exclude: ['deletedAt']
                }
            });

            rep.status(200).json({ result: results, error: '' });
        } catch (error) {
            logger.error(error);
            rep.status(500).json({ result: false, error: 'Erreur interne' });
        }
    },

    add: async function (req, rep) {
        try {
            let { title, userId } = req.body;

            if (!title) {
                rep.status(400).json({ result: false, error: 'Le tableau de bord doit avoir un titre.' });
            } else {
                let newDashboard = {
                    title: title,
                    userId: userId
                };

                await models.dashboards.create(newDashboard);
                rep.status(200).json({ result: true, error: '' });
            }
        } catch (error) {
            logger.error(error);
            rep.status(500).json({ result: false, error: 'Erreur interne' });
        }
    },

    updateById: async function (req, rep) {
        try {
            let data = await models.dashboards.findOne({ where: { id: req.params.id } });

            if (data) {
                await data.update(req.body);
                rep.status(200).json({ result: true, error: '' });
            } else {
                rep.status(404).json({ result: false, error: 'Aucun résultat pour cet identifiant.' });
            }

            await data.save();
        } catch (error) {
            logger.error(error);
            rep.status(500).json({ result: false, error: 'Erreur interne' });
        }
    },

    deleteById: async function (req, rep) {
        try {
            let data = await models.dashboards.findOne({ where: { id: req.params.id } });

            if (data) {
                await models.dashboards.destroy({ where: { id: req.params.id } });
                rep.status(200).json({ result: true, error: '' });
            } else {
                rep.status(404).json({ result: false, error: 'Aucun résultat pour cet identifiant.' });
            }
        } catch (error) {
            logger.error(error);
            rep.status(500).json({ result: false, error: 'Erreur interne' });
        }
    },

    getColumnsByDashboard: async function (req, rep) {
        try {
            let columnsByDashboard = await models.columns.findAll({
                where: {
                    dashboardId: req.params.dashboardId
                },
                order: [['title', 'ASC']],
                attributes: {
                    exclude: ['deletedAt']
                }
            });

            rep.status(200).json({ result: columnsByDashboard, error: '' });
        } catch (error) {
            logger.error(error);
            rep.status(500).json({ result: false, error: 'Erreur interne' });
        }
    },

    getColumnsByDashboardWithTasks: async function (req, rep) {
        try {
            let columnsByDashboardWithTasks = await models.columns.findAll({
                where: {
                    dashboardId: req.params.dashboardId
                },
                order: [['title', 'ASC']],
                attributes: {
                    exclude: ['deletedAt']
                },
                include: {
                    model: models.tasks,
                    attributes: {
                        exclude: ['deletedAt']
                    }
                }
            });

            columnsByDashboardWithTasks.forEach(column => column.tasks.forEach(task => 
                    task.image = task.image !== null ? true : false
                )
            );
            rep.status(200).json({ result: columnsByDashboardWithTasks, error: '' });
        } catch (error) {
            logger.error(error);
            rep.status(500).json({ result: false, error: 'Erreur interne' });
        }
    }
}

module.exports = Controller;