const fs = require('fs');
const models = require('../models');
const logger = require('../services/logger');

let Controller = {
    getAll: async function (req, rep) {
        try {
            let results = await models.tasks.findAll({
                order: [['title', 'ASC']],
                attributes: {
                    exclude: ['deletedAt']
                }
            });

            results.forEach(img => img.image = img.image !== null ? true : false);
            rep.status(200).json({ result: results, error: '' });
        } catch (error) {
            logger.error(error);
            rep.status(500).json({ result: false, error: 'Erreur interne' });
        }
    },

    add: async function (req, rep) {
        try {
            let { title, content, columnId } = req.body;
            let isImage = req.files?.length > 0;

            if (!title) {
                rep.status(400).json({ result: false, error: 'La tâche doit avoir un titre.' });
            } else {
                let newTask = {
                    title: title,
                    content: content,
                    columnId: columnId
                };

                if (isImage) {
                    newTask.image = fs.readFileSync(req.files[0].path);
                    fs.unlinkSync(req.files[0].path);
                }

                await models.tasks.create(newTask);
                rep.status(200).json({ result: true, error: '' });
            }
        } catch (error) {
            logger.error(error);
            rep.status(500).json({ result: false, error: 'Erreur interne' });
        }
    },

    updateById: async function (req, rep) {
        try {
            let { title, content, columnId } = req.body;

            if (!title) {
                rep.status(400).json({ result: false, error: 'La tâche doit avoir un titre.'});
            }

            let data = await models.tasks.findOne({ where: { id: req.params.id } });

            if (data) {
                if (title) data.title = title;
                if (content) data.content = content;
                if (columnId) data.columnId = columnId;
                if (req.files?.length > 0) {
                    data.image = fs.readFileSync(req.files[0].path);
                    fs.unlinkSync(req.files[0].path);
                } else {
                    data.image = null;
                }

                await data.save();
                rep.status(200).json({ result: true, error: '' });
            } else {
                rep.status(404).json({ result: false, error: 'Aucun résultat pour cet identifiant.' });
            }

            await data.save();
        } catch (error) {
            logger.error(error);
            rep.status(500).json({ result: false, error: 'Erreur interne' });
        }
    },

    deleteById: async function (req, rep) {
        try {
            let data = await models.tasks.findOne({ where: { id: req.params.id } });

            if (data) {
                await models.tasks.destroy({ where: { id: req.params.id } });
                rep.status(200).json({ result: true, error: '' });
            } else {
                rep.status(404).json({ result: false, error: 'Aucun résultat pour cet identifiant.' });
            }
        } catch (error) {
            logger.error(error);
            rep.status(500).json({ result: false, error: 'Erreur interne' });
        }
    },

    getImage: async function (req, rep) {
        try {
            let result = await models.tasks.findOne({ where: { id: req.params.id } });

            rep.contentType('image/*');
            rep.send(result.image);
        } catch (error) {
            logger.error(error);
            rep.status(500).json({ result: false, error: 'Erreur interne' });
        }
    }
}

module.exports = Controller;