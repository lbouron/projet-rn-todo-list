const models = require('../models');
// const bcryptUtils = require('../utils/bcrypt');

let Controller = {
    getAll: async function (req, rep) {
        try {
            let results = await models.users.findAll({
                order: [['email', 'ASC']],
                attributes: {
                    exclude: ['deletedAt']
                }
            });

            rep.status(200).json({ result: results, error: '' });
        } catch (error) {
            rep.status(500).json({ result: false, error: 'Erreur interne' });
        }
    },

    getDashboardsByUser: async function (req, rep) {
        try {
            let results = await models.dashboards.findAll({ where: { userId: req.user.id } });

            rep.status(200).json({ result: results, error: '' });
        } catch (error) {
            rep.status(500).json({ result: false, error: 'Erreur interne' });
        }
    }
};

module.exports = Controller;