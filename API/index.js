const express = require('express');
const helmet = require('helmet');
const morgan = require('morgan');
const passport = require('passport');
const swaggerJsDoc = require('swagger-jsdoc');
const swaggerUi = require('swagger-ui-express');
const session = require('express-session');
// const bodyParser = require('body-parser');
// const multer = require('multer');
const config = require('./config/config');
const cors = require('cors');
const glob = require('glob');
const path = require('path');
const apiBasePath = 'api';
const port = config.api.port;

const swaggerOptions = {
    swaggerDefinition: {
        info: {
            title: 'Projet Todo List',
            description: 'API Projet Todo List',
            // servers: [`http://localhost:${port}`]
        },
        basePath: `/${apiBasePath}`
    },
    apis: ['./routes/*.js']
}

const swaggerDocs = swaggerJsDoc(swaggerOptions);
const passportStrategy = require('./services/passport');

// Initialisations
let app = express();
app.use(session({ secret: config.keys.secret, resave: false, saveUninitialized: false }));
app.use(express.static(path.join(__dirname, 'public')));
app.use(helmet());

// CORS avec le backend
/*app.use(function (req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', process.env.AUTH_BACKEND_URL || 'http://localhost:4001');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
    res.setHeader('Access-Control-Allow-Credentials', true);
    next();
});*/
app.use(cors({ credentials: true, origin: config.api.backendUrl }));

// HTTP logger
app.use(express.urlencoded({ extended: true }));
app.use(express.json());
// app.use(multer());
app.use(morgan('dev'));

// Passport
app.use(passport.initialize());
app.use(passport.session());
// Hook passport local strategy
passportStrategy.hookLocalStrategy(passport);

// Swagger
if (config.api.nodeEnv !== 'production') {
    app.use(`/${apiBasePath}/docs`, swaggerUi.serve, swaggerUi.setup(swaggerDocs));
}

// Routes de l'API
glob.sync('./routes/**/*.js').forEach(function (file) {
    app.use(`/${apiBasePath}`, require(path.resolve(file))(passport));
});

app.listen(port, function () {
    console.log(`Server started on port ${port}`);
});