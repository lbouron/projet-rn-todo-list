const Sequelize = require('sequelize');
const db = require('../services/sequelize');
const Task = require('../models/tasks');
const Dashboard = require('../models/dashboards');

let modelDefinition = {
    id: {
        type: Sequelize.BIGINT,
        primaryKey: true,
        unique: true,
        autoIncrement: true,
        allowNull: false
    },
    title: {
        type: Sequelize.STRING,
        allowNull: false
    },
    dashboardId: {
        type: Sequelize.BIGINT,
        allowNull: false
    }
};

let modelOptions = {
    freezeTableName: true,
    paranoid: true,
    timestamps: true
};

let Model = db.define('columns', modelDefinition, modelOptions);
Model.hasMany(Task);
// Model.belongsTo(Dashboard);

module.exports = Model;