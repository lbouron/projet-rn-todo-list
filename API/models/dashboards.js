const Sequelize = require('sequelize');
const db = require('../services/sequelize');
const Column = require('../models/columns');
const User = require('../models/users');

let modelDefinition = {
    id: {
        type: Sequelize.BIGINT,
        primaryKey: true,
        unique: true,
        autoIncrement: true,
        allowNull: false
    },
    title: {
        type: Sequelize.STRING,
        allowNull: false
    },
    userId: {
        type: Sequelize.STRING,
        allowNull: false
    }
};

let modelOptions = {
    freezeTableName: true,
    paranoid: true,
    timestamps: true
};

let Model = db.define('dashboards', modelDefinition, modelOptions);
Model.hasMany(Column);
Model.belongsTo(User);

module.exports = Model;