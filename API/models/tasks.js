const Sequelize = require('sequelize');
const db = require('../services/sequelize');
const Column = require('../models/columns');

let modelDefinition = {
    id: {
        type: Sequelize.BIGINT,
        primaryKey: true,
        unique: true,
        autoIncrement: true,
        allowNull: false
    },
    title: {
        type: Sequelize.STRING,
        allowNull: false
    },
    content: {
        type: Sequelize.TEXT,
        allowNull: true
    },
    image: {
        type: Sequelize.BLOB,
        allowNull: true
    },
    columnId: {
        type: Sequelize.BIGINT,
        allowNull: false
    }
};

let modelOptions = {
    freezeTableName: true,
    paranoid: true,
    timestamps: true
};

let Model = db.define('tasks', modelDefinition, modelOptions);
// Model.belongsTo(Column);

module.exports = Model;