const Sequelize = require('sequelize');
const bcryptUtils = require('../utils/bcrypt');
const db = require('../services/sequelize');
const models = require('./index');

let modelDefinition = {
    id: {
        type: Sequelize.STRING,
        defaultValue: Sequelize.DataTypes.UUIDV1,
        primaryKey: true,
        unique: true,
        allowNull: false
    },
    email: {
        type: Sequelize.STRING,
        allowNull: false
    },
    password: {
        type: Sequelize.STRING,
        allowNull: true
    }
};

let modelOptions = {
    instanceMeethods: {
        comparePasswords: bcryptUtils.comparePasswords
    },
    hooks: {
        beforeValidate: bcryptUtils.hashPassword
    },
    freezeTableName: true,
    paranoid: true,
    timestamps: true
};

let Model = db.define('users', modelDefinition, modelOptions);

module.exports = Model;