let router = require('express').Router();

const config = require('../config/config');
const allowOnly = require('../services/route').allowOnly;

const Controller = require('../controllers/columns');

let APIRoutes = function (passport) {
    /**
     * @swagger
     * /columns:
     *   get:
     *     tags:
     *       - Colonnes
     *     summary: Retourne toutes les entrées
     *     description: Retourne toutes les entrées
     *     produces:
     *       - application/json
     *     responses:
     *       200:
     *         description: Succès
     *       401:
     *         description: Utilisateur non connecté
     *       500:
     *         description: Erreur interne
     */
    router.get('/columns', allowOnly(config.userRoles.user, Controller.getAll));

    /**
     * @swagger
     * /columns/add:
     *   put:
     *     tags:
     *       - Colonnes
     *     summary: Ajoute une entrée
     *     description: Ajoute une entrée
     *     produces:
     *       - application/json
     *     parameters:
     *       - in: body
     *         name: entry
     *         description: Entrée à ajouter
     *         schema:
     *           type: object
     *           properties:
     *             title:
     *               type: string
     *             dashboardId:
     *               type: string
     *     responses:
     *       200:
     *         description: Succès
     *       400:
     *         description: Champs obligatoires manquants
     *       401:
     *         description: Utilisateur non connecté
     *       500:
     *         description: Erreur interne
     */
    router.put('/columns/add', allowOnly(config.userRoles.user, Controller.add));

    /**
     * @swagger
     * /columns/update/{id}:
     *   post:
     *     tags:
     *       - Colonnes
     *     summary: Met à jour une entrée
     *     description: Met à jour une entrée
     *     produces:
     *       - application/json
     *     parameters:
     *       - in: body
     *         name: entry
     *         description: Entrée à ajouter
     *         schema:
     *           type: object
     *           properties:
     *             title:
     *               type: string
     *     responses:
     *       200:
     *         description: Succès
     *       401:
     *         description: Utilisateur non connecté
     *       404:
     *         description: L'entrée n'existe pas
     *       500:
     *         description: Erreur interne
     */
    router.post('/columns/update/:id', allowOnly(config.userRoles.user, Controller.updateById));

    /**
     * @swagger
     * /columns/delete/{id}:
     *   delete:
     *     tags:
     *       - Colonnes
     *     summary: Supprime une entrée
     *     description: Supprime une entrée
     *     produces:
     *       - application/json
     *     parameters:
     *       - in: params
     *         name: entry
     *         description: Entrée à supprimer
     *         type: integer
     *     responses:
     *       200:
     *         description: Succès
     *       401:
     *         description: Utilisateur non connecté
     *       404:
     *         description: L'entrée n'existe pas
     *       500:
     *         description: Erreur interne
     */
    router.delete('/columns/delete/:id', allowOnly(config.userRoles.user, Controller.deleteById));

    return router;
};

module.exports = APIRoutes;