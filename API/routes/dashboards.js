let router = require('express').Router();

const config = require('../config/config');
const allowOnly = require('../services/route').allowOnly;

const Controller = require('../controllers/dashboards');

let APIRoutes = function (passport) {
    /**
     * @swagger
     * /dashboards:
     *   get:
     *     tags:
     *       - Tableaux de bord
     *     summary: Retourne toutes les entrées
     *     description: Retourne toutes les entrées
     *     produces:
     *       - application/json
     *     responses:
     *       200:
     *         description: Succès
     *       401:
     *         description: Utilisateur non connecté
     *       500:
     *         description: Erreur interne
     */
    router.get('/dashboards', allowOnly(config.userRoles.user, Controller.getAll));

    /**
     * @swagger
     * /dashboards/add:
     *   put:
     *     tags:
     *       - Tableaux de bord
     *     summary: Ajoute une entrée
     *     description: Ajoute une entrée
     *     produces:
     *       - application/json
     *     parameters:
     *       - in: body
     *         name: entry
     *         description: Entrée à ajouter
     *         schema:
     *           type: object
     *           properties:
     *             title:
     *               type: string
     *             userId:
     *               type: string
     *     responses:
     *       200:
     *         description: Succès
     *       400:
     *         description: Champs obligatoires manquants
     *       401:
     *         description: Utilisateur non connecté
     *       500:
     *         description: Erreur interne
     */
    router.put('/dashboards/add', allowOnly(config.userRoles.user, Controller.add));

    /**
     * @swagger
     * /dashboards/update/{id}:
     *   post:
     *     tags:
     *       - Tableaux de bord
     *     summary: Met à jour une entrée
     *     description: Met à jour une entrée
     *     produces:
     *       - application/json
     *     parameters:
     *       - in: body
     *         name: entry
     *         description: Entrée à ajouter
     *         schema:
     *           type: object
     *           properties:
     *             title:
     *               type: string
     *     responses:
     *       200:
     *         description: Succès
     *       401:
     *         description: Utilisateur non connecté
     *       404:
     *         description: L'entrée n'existe pas
     *       500:
     *         description: Erreur interne
     */
    router.post('/dashboards/update/:id', allowOnly(config.userRoles.user, Controller.updateById));

    /**
     * @swagger
     * /dashboards/delete/{id}:
     *   delete:
     *     tags:
     *       - Tableaux de bord
     *     summary: Supprime une entrée
     *     description: Supprime une entrée
     *     produces:
     *       - application/json
     *     parameters:
     *       - in: params
     *         name: entry
     *         description: Entrée à supprimer
     *         type: integer
     *     responses:
     *       200:
     *         description: Succès
     *       401:
     *         description: Utilisateur non connecté
     *       404:
     *         description: L'entrée n'existe pas
     *       500:
     *         description: Erreur interne
     */
    router.delete('/dashboards/delete/:id', allowOnly(config.userRoles.user, Controller.deleteById));

    /**
     * @swagger
     * /dashboards/{dashboardId}/columns:
     *   get:
     *     tags:
     *       - Tableaux de bord
     *     summary: Retourne toutes les colonnes d'un tableau de bord
     *     description: Retourne toutes les colonnes d'un tableau de bord
     *     produces:
     *       - application/json
     *     parameters:
     *       - name: id
     *         description: Identifiant du tableau de bord
     *         in: body
     *         required: true
     *         type: int
     *     responses:
     *       200:
     *         description: Succès
     *       401:
     *         description: Utilisateur non connecté
     *       500:
     *         description: Erreur interne
     */
    router.get('/dashboards/:dashboardId/columns', allowOnly(config.userRoles.user, Controller.getColumnsByDashboard));

    /**
     * @swagger
     * /dashboards/{dashboardId}/columnswithtasks:
     *   get:
     *     tags:
     *       - Tableaux de bord
     *     summary: Retourne toutes les colonnes d'un tableau de bord avec les tâches
     *     description: Retourne toutes les colonnes d'un tableau de bord avec les tâches
     *     produces:
     *       - application/json
     *     parameters:
     *       - name: id
     *         description: Identifiant du tableau de bord
     *         in: body
     *         required: true
     *         type: int
     *     responses:
     *       200:
     *         description: Succès
     *       401:
     *         description: Utilisateur non connecté
     *       500:
     *         description: Erreur interne
     */
    router.get('/dashboards/:dashboardId/columnswithtasks', allowOnly(config.userRoles.user, Controller.getColumnsByDashboardWithTasks));

    return router;
};

module.exports = APIRoutes;