let router = require('express').Router();

const config = require('../config/config');
const upload = require('../services/upload');
const allowOnly = require('../services/route').allowOnly;

const Controller = require('../controllers/tasks');

let APIRoutes = function (passport) {
    /**
     * @swagger
     * /tasks:
     *   get:
     *     tags:
     *       - Tâches
     *     summary: Retourne toutes les entrées
     *     description: Retourne toutes les entrées
     *     produces:
     *       - application/json
     *     responses:
     *       200:
     *         description: Succès
     *       401:
     *         description: Utilisateur non connecté
     *       500:
     *         description: Erreur interne
     */
    router.get('/tasks', allowOnly(config.userRoles.user, Controller.getAll));

    /**
     * @swagger
     * /tasks/add:
     *   put:
     *     tags:
     *       - Tâches
     *     summary: Ajoute une entrée
     *     description: Ajoute une entrée
     *     produces:
     *       - application/json
     *     parameters:
     *       - in: body
     *         name: entry
     *         description: Entrée à ajouter
     *         schema:
     *           type: object
     *           properties:
     *             title:
     *               type: string
     *             content:
     *               type: text
     *             columnId:
     *               type: bigint
     *     responses:
     *       200:
     *         description: Succès
     *       400:
     *         description: Champs obligatoires manquants
     *       401:
     *         description: Utilisateur non connecté
     *       500:
     *         description: Erreur interne
     */
    router.put('/tasks/add', upload.any(), allowOnly(config.userRoles.user, Controller.add));

    /**
     * @swagger
     * /tasks/update/{id}:
     *   post:
     *     tags:
     *       - Tâches
     *     summary: Met à jour une entrée
     *     description: Met à jour une entrée
     *     produces:
     *       - application/json
     *     parameters:
     *       - in: body
     *         name: entry
     *         description: Entrée à ajouter
     *         schema:
     *           type: object
     *           properties:
     *             title:
     *               type: string
     *             content:
     *               type: text
     *             columnId:
     *               type: bigint
     *     responses:
     *       200:
     *         description: Succès
     *       401:
     *         description: Utilisateur non connecté
     *       404:
     *         description: L'entrée n'existe pas
     *       500:
     *         description: Erreur interne
     */
    router.post('/tasks/update/:id', upload.any(), allowOnly(config.userRoles.user, Controller.updateById));

    /**
     * @swagger
     * /tasks/delete/{id}:
     *   delete:
     *     tags:
     *       - Tâches
     *     summary: Supprime une entrée
     *     description: Supprime une entrée
     *     produces:
     *       - application/json
     *     parameters:
     *       - in: params
     *         name: entry
     *         description: Entrée à supprimer
     *         type: integer
     *     responses:
     *       200:
     *         description: Succès
     *       401:
     *         description: Utilisateur non connecté
     *       404:
     *         description: L'entrée n'existe pas
     *       500:
     *         description: Erreur interne
     */
    router.delete('/tasks/delete/:id', allowOnly(config.userRoles.user, Controller.deleteById));

    /**
     * @swagger
     * /tasks/image/{id}:
     *   get:
     *     tags:
     *       - Tâches
     *     summary: Retourne l'image d'une tâche
     *     description: Retourne l'image d'une tâche
     *     produces:
     *       - application/json
     *     parameters:
     *       - in: params
     *         name: id
     *         description: Entrée désirée
     *         type: integer
     *     responses:
     *       200:
     *         description: Succès
     *       401:
     *         description: Utilisateur non connecté
     *       500:
     *         description: Erreur interne
     */
    router.get('/tasks/image/:id', Controller.getImage);

    return router;
};

module.exports = APIRoutes;