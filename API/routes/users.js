let router = require('express').Router();

const config = require('../config/config');
const allowOnly = require('../services/route').allowOnly;

const Controller = require('../controllers/users');

let APIRoutes = function (passport) {
    /**
     * @swagger
     * /users:
     *   get:
     *     tags:
     *       - Utilisateurs
     *     summary: Retourne toutes les entrées (filtres= ?order=email&sort=ASC)
     *     description: Retourne toutes les entrées
     *     produces:
     *       - application/json
     *     responses:
     *       200:
     *         description: Liste
     *       500:
     *         description: Erreur interne
     */
    router.get('/users', Controller.getAll);

    /**
     * @swagger
     * /users/{userId}/dashboards:
     *   get:
     *     tags:
     *       - Utilisateurs
     *     summary: Retourne tous les tableaux de bord d'un utilisateur
     *     description: Retourne tous les tableaux de bord d'un utilisateur
     *     produces:
     *       - application/json
     *     parameters:
     *       - name: id
     *         description: Identifiant de l'utilisateur
     *         in: body
     *         required: true
     *         type: int
     *     responses:
     *       200:
     *         description: Succès
     *       401:
     *         description: Utilisateur non connecté
     *       500:
     *         description: Erreur interne
     */
    router.get('/users/:userId/dashboards', allowOnly(config.userRoles.user, Controller.getDashboardsByUser));

    return router;
};

module.exports = APIRoutes;