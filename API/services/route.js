const services = require('./');

exports.allowOnly = function (requiredPermission, callback) {
    function checkUserRole(req, rep) {
        if (req.user) {
            services.logger.info(`Request from \x1b[32m${req.user.email} \x1b[36mon route with \x1b[35m"${requiredPermission}" \x1b[36mpermission`);

            let isAllowed = false;

            if (typeof requiredPermission === 'string') {
                if ((requiredPermission === 'user' && req.user) || req.user[requiredPermission]) {
                    isAllowed = true;
                }
            } else {
                requiredPermission.forEach(permission => {
                    if ((permission === 'user' && req.user) || req.user[permission]) {
                        isAllowed = true;
                    }
                });
            }

            if (isAllowed) {
                callback(req, rep);
            } else {
                rep.sendStatus(403);
            }
        } else {
            rep.sendStatus(401);
        }
    }

    return checkUserRole;
};