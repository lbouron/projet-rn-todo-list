const config = require('../config/config');
const Sequelize = require('sequelize');

const sequelize = new Sequelize(
    config.db.name,
    config.db.user,
    config.db.password,
    config.db.details
);

sequelize.sync({ alter: true }).then(result => {
    //console.log(result);
}).catch(err => console.log(err));

module.exports = sequelize;