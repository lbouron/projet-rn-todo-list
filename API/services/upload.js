const multer = require('multer');
const tempDirectory = require('temp-dir');

const imageFilter = (req, file, cb) => {
    if (file.mimetype.includes('image')) {
        cb(null, true);
    } else {
        cb('Vous ne pouvez importer que des images.', false);
    }
};

let storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, tempDirectory);
    },
    filename: (req, file, cb) => {
        cb(null, `${file.originalname}`);
    },
});

let upload = multer({ storage: storage, fileFilter: imageFilter });
module.exports = upload;