import React, { useEffect, useState } from 'react';
import { Image } from 'react-native';
import * as Font from 'expo-font';
import { Asset } from 'expo-asset';
import AppLoading from 'expo-app-loading';
import { Block, GalioProvider } from 'galio-framework';
import { NavigationContainer } from '@react-navigation/native';

// Before rendering any navigation stack
import { enableScreens } from 'react-native-screens';
enableScreens();

import Screens from './navigation/Screens';
import { theme as argonTheme, images } from './constants';

// cache app images
const assetImages = [];

export default function App() {
  const [isLoadingComplete, setIsLoadingComplete] = useState(false);

  useEffect(() => {
    async function initAppAsync() {
      // On met les images dans la liste des images à charger
      for (var image in images) {
        assetImages.push(images[image]);
      }
    }
    initAppAsync();
  }, []);

  // Fonction de mise en cache des fonts
  cacheFonts = () => {
    return Font.loadAsync({
      'open-sans': require('./assets/font/OpenSans-Regular.ttf'),
      'open-sans-light': require('./assets/font/OpenSans-Light.ttf'),
      'open-sans-bold': require('./assets/font/OpenSans-Bold.ttf')
    });
  }

  // Fonction de mise en cache des images
  cacheImages = (imagesToCache) => {
    return imagesToCache.map(image => {
      if (typeof image === 'string') {
        return Image.prefetch(image);
      } else {
        return Asset.fromModule(image).downloadAsync();
      }
    });
  }

  _loadResourcesAsync = async () => {
    // On lance le cache des images et des fonts au chargement des ressources
    return Promise.all([
      ...cacheImages(assetImages),
      cacheFonts()
    ]);
  };

  _handleLoadingError = error => {
    // In this case, you might want to report the error to your error
    // reporting service, for example Sentry
    // console.warn(error);
  };

  _handleFinishLoading = async () => {
    setIsLoadingComplete(true);
  };

  if (!isLoadingComplete) {
    return (
        <AppLoading
          startAsync={_loadResourcesAsync()}
          onError={_handleLoadingError()}
          onFinish={_handleFinishLoading()}
        />
    );
  } else {
    return (
      <NavigationContainer>
        <GalioProvider theme={argonTheme}>
          <Block flex>
            <Screens />
          </Block>
        </GalioProvider>
      </NavigationContainer>
    );
  }
}