import React from 'react';
import { StyleSheet, Dimensions } from 'react-native';

import { theme } from '../../constants';

const { width, height } = Dimensions.get('screen');

export const styles = StyleSheet.create({
    // CSS Général
    container: {
        width: width,
        height: height,
        backgroundColor: theme.COLORS.WHITE,
        paddingTop: 10
    },
    banniere: {
        width: width,
        height: height / 3
    },
    button: {
        width: 'auto',
        height: 'auto',
        margin: 0,
        padding: 10
    },
    input: {
        width: width * 0.9,
        height: 60,
        backgroundColor: 'rgba(210, 104, 204, 0.6)',
        marginVertical: 0,
        paddingLeft: 120,
        borderRadius: 0,
        borderColor: theme.COLORS.BORDER_COLOR
    },

    // CSS Connexion / Inscription
    connexion: {
        flex: 1,
        width: width,
        height: '100%',
        backgroundColor: theme.COLORS.WHITE
    },
    google: {
        height: 'auto',
        width: 'auto',
        padding: 15,
        marginVertical: -30
    },

    // CSS Card
    card: {
        height: 50,
        width: width - 20,
        marginVertical: 5,
        marginHorizontal: 0,
        padding: 0
    },
    subcard: {
        width: '100%',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingHorizontal: 10
    },

    // CSS Sous-Card
    taches: {
        width: width - 60,
        backgroundColor: theme.COLORS.DISABLED,
        marginHorizontal: 10,
        marginVertical: 5,
        padding: 10,
        borderRadius: 5,
        borderColor: theme.COLORS.BLACK,
        borderWidth: 1,
        elevation: 0,
        shadowOpacity: 0
    },

    // CSS Accordion
    accordion: {
        height: 50,
        backgroundColor: theme.COLORS.PRIMARY,
        marginTop: 5,
        marginHorizontal: 15,
        padding: 10,
        justifyContent: 'center',
        borderRadius: 5,
        zIndex: 2
    },
    tachesderoulees: {
        height: 'auto',
        backgroundColor: theme.COLORS.DISABLED,
        marginTop: -5,
        marginBottom: 5,
        marginHorizontal: 15,
        paddingVertical: 10,
        alignItems: 'center',
        zIndex: 1
    },

    // CSS Bouton New / Edit
    formbutton: {
        width: width * 0.9,
        backgroundColor: theme.COLORS.PRIMARY,
        marginHorizontal: 0,
        marginVertical: 10
    },
    image: {
        width: width - 40,
        height: 250,
        borderRadius: 20,
        borderColor: theme.COLORS.PRIMARY,
        borderStyle: 'dashed',
        borderWidth: 1
    },
    modal: {
        backgroundColor: 'rgba(0, 0, 0, 0.7)',
        height: '80%',
        marginTop: 'auto',
        marginBottom: 'auto',
        borderRadius: 20,
        padding: 20
    },

    // CSS Tâches
    content: {
        height: 250,
        marginVertical: 5,
        paddingHorizontal: 10,
        borderRadius: 10,
        borderColor: theme.COLORS.PRIMARY,
        borderWidth: 1
    }
});