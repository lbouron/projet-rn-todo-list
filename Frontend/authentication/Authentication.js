import { Alert } from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';

import todolistApiService from '../services/todolistApiService';
import connectedProfile from './ConnectedProfile';
import { storage } from '../constants';
import { apiLogger, appLogger } from '../services/logger';
import Utils from '../utils/Utils';
import config from '../config';

async function Register(user, navigation) {
    let success = true;

    try {
        await todolistApiService.request({
            url: '/register',
            method: 'PUT',
            data: user
        });

        apiLogger.success(user);
    } catch (error) {
        success = false;

        if (error.message.endsWith('409')) {
            Alert.alert('Todo List', 'Cette adresse email est déjà utilisée.', [{ text: 'Fermer', style: 'cancel' }]);
            apiLogger.error('L\'adresse email est déjà utilisée.');
        } else {
            Alert.alert('Todo List', 'Il y a eu une erreur lors de l\'inscription.');
            apiLogger.error('Erreur lors de l\'inscription.');
            apiLogger.error(error);
        }
    }

    return success;
}

async function Login(email, password, showAlert = true) {
    let success = true;

    try {
        await todolistApiService.request({
            url: '/signin',
            method: 'POST',
            data: {
                email: email,
                password: password
            }
        });

        await AsyncStorage.setItem(storage.UserEmail, email);
        await AsyncStorage.setItem(storage.UserPassword, password);

        await UserInfo();
        await Utils.getDashboardsByUser();
    } catch (error) {
        success = false;

        if (error.message.endsWith('401')) {
            await AsyncStorage.multiRemove([storage.UserEmail, storage.UserPassword]);

            if (showAlert) { Alert.alert('Todo List', 'Adresse email ou mot de passe incorrect.') };
            apiLogger.error('Erreur lors de la connexion avec email / mot de passe.');
            apiLogger.error(error);
        } else {
            if (showAlert) { Alert.alert('Todo List', 'Il y a eu une erreur lors de la connexion. Veuillez réessayer.') };
            apiLogger.error('Erreur lors de la connexion avec email / mot de passe.');
            apiLogger.error(error);
        }
    }

    return success;
}

async function UserInfo() {
    try {
        const userInfos = await todolistApiService.request({
            url: 'userInfo',
            method: 'GET'
        });

        connectedProfile.id = userInfos.data.id;
        connectedProfile.email = userInfos.data.email;
        connectedProfile.firstname = userInfos.data.name;

        apiLogger.success(userInfos.data.email + ' (' + userInfos.data.id + ')');
    } catch (error) {
        apiLogger.error('Erreur lors de la récupération des informations de l\'utilisateur.');
        apiLogger.error(error);
    }
}

async function Logout() {
    try {
        await todolistApiService.request({
            url: '/logout',
            method: 'GET'
        });

        await AsyncStorage.multiRemove([
            storage.UserEmail,
            storage.UserPassword
        ]);

        apiLogger.success('Vous êtes bien déconnecté(e).');
    } catch (error) {
        appLogger.error(error);
    }
}

export default {
    Register,
    Login,
    UserInfo,
    Logout
}