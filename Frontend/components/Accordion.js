import React from 'react';
import { Block, Text } from 'galio-framework';

import Icon from './Icon';
import { fontsize, theme } from '../constants';
import { styles } from '../assets/css/styles';

export default class Accordion extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            deroule: false
        }
    }

    clickDeroule = (deroule) => {
        this.setState({ deroule: deroule });
    }

    render() {
        const { column, createTask, editColumn, deleteColumn, titleTasks } = this.props;

        return (
            <Block>
                <Block style={styles.accordion}>
                    <Block row style={{ justifyContent: 'space-between' }}>
                        <Text
                            size={fontsize.body}
                            color={theme.COLORS.WHITE}
                            numberOfLines={1}
                            style={{ width: '45%' }}
                        >
                            {column.title}
                        </Text>

                        <Block row width={70} style={{ justifyContent: 'space-around', alignItems: 'center' }}>
                            <Icon
                                name='plus'
                                family='font-awesome'
                                size={fontsize.body}
                                color={theme.COLORS.WHITE}
                                onPress={createTask}
                            />

                            <Icon
                                name='edit'
                                family='font-awesome'
                                size={fontsize.body}
                                color={theme.COLORS.WHITE}
                                onPress={editColumn}
                            />
                            
                            <Icon
                                name='trash-o'
                                family='font-awesome'
                                size={fontsize.body}
                                color={theme.COLORS.WHITE}
                                onPress={deleteColumn}
                            />
                        </Block>

                            <Icon
                                name={this.state.deroule ? 'chevron-up' : 'chevron-down'}
                                family='font-awesome'
                                size={fontsize.body}
                                color={theme.COLORS.WHITE}
                                onPress={() => this.clickDeroule(!this.state.deroule)}
                            />
                    </Block>
                </Block>

                {this.state.deroule === true && <Block style={styles.tachesderoulees}>
                    {titleTasks}
                </Block>}
            </Block>
        )
    }
}