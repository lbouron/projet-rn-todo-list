import React from 'react';
import { Block, Text } from 'galio-framework';

import Button from './Button';
import Icon from './Icon';
import { fontsize, theme } from '../constants';
import { styles } from '../assets/css/styles';

import * as WebBrowser from 'expo-web-browser';
import * as Google from 'expo-auth-session/providers/google';

WebBrowser.maybeCompleteAuthSession();

export default function AuthSocial() {
    const [ request, response, promptAsync ] = Google.useAuthRequest({
        expoClientId: '780947518580-8gsljgtg1r0s4f3eb74qhrcm6e5km51c.apps.googleusercontent.com',
        iosClientId: '780947518580-8dfeqak9hrb2sojoh3clc4351f1i49s4.apps.googleusercontent.com',
        // androidClientId: 'GOOGLE_GUID.apps.googleusercontent.com',
        webClientId: '780947518580-rhm6sl6s2fk32nim4mogrg5svae00cai.apps.googleusercontent.com',
    });

    React.useEffect(() => {
        if (response?.type === 'success') {
            const { authentication } = response;
        }
    }, [response]);

    return (
        <Button shadowless
            disabled={!request}
            onPress={() => promptAsync()}
            style={styles.google}
        >
            <Icon
                name='google'
                family='font-awesome'
                color={theme.COLORS.WHITE}
                size={25}
            />
        </Button>
    );
}