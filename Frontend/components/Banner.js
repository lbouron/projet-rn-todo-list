import React from 'react';
import { Image } from 'react-native';

import { styles } from '../assets/css/styles';

export default class Banner extends React.Component {
    render() {
        const { images } = this.props;

        return (
            <Image
                source={images}
                style={styles.banniere}
                resizeMode='contain'
            />
        );
    }
}