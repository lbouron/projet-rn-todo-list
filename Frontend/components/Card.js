import React from 'react';
import { Block, Text } from 'galio-framework';

import Button from './Button';
import Icon from './Icon';
import { fontsize, theme } from '../constants';
import { styles } from '../assets/css/styles';

export default class Card extends React.Component {
    render() {
        const { dashboard, onPress, editDashboard, deleteDashboard } = this.props;
        
        return (
            <Button shadowless style={styles.card} onPress={onPress}>
                <Block row style={styles.subcard}>
                    <Text
                        size={fontsize.body}
                        color={theme.COLORS.WHITE}
                        numberOfLines={1}
                        style={{ width: '45%' }}
                    >
                        {dashboard.title}
                    </Text>

                    <Block row width={50} style={{ justifyContent: 'space-around', alignItems: 'center' }}>
                        <Icon
                            name='edit'
                            family='font-awesome'
                            size={fontsize.body}
                            color={theme.COLORS.WHITE}
                            onPress={editDashboard}
                        />
                        
                        <Icon
                            name='trash-o'
                            family='font-awesome'
                            size={fontsize.body}
                            color={theme.COLORS.WHITE}
                            onPress={deleteDashboard}
                        />
                    </Block>

                    <Icon
                        name='chevron-right'
                        family='font-awesome'
                        size={fontsize.body}
                        color={theme.COLORS.WHITE}
                    />
                </Block>
            </Button>
        );
    }
}