import React from 'react';
import { StyleSheet } from 'react-native';
import { Block, NavBar, theme } from 'galio-framework';
import { withNavigation } from '@react-navigation/compat';
import { CommonActions } from '@react-navigation/native';

import Icon from './Icon';

function Header(props) {
    handleLeftPress = () => {
        return (props.back ? props.navigation.dispatch(CommonActions.goBack()) : props.navigation.openDrawer());
    }

    const headerStyles = [
        !props.noShadow ? styles.shadow : null,
        props.transparent ? { backgroundColor: 'rgba(0, 0, 0, 0)' } : null,
    ];

    const navbarStyles = [
        styles.navbar,
        { backgroundColor: theme.COLORS.WHITE },
        props.bgColor && { backgroundColor: props.bgColor }
    ];

    return (
        <Block safe style={headerStyles}>
            <NavBar
                back={false}
                title={props.title}
                style={navbarStyles}
                transparent={props.transparent}
                onLeftPress={handleLeftPress}
                left={
                    props.back && (
                        <Icon
                            name={'nav-left'}
                            family='ArgonExtra'
                            size={20}
                            onPress={handleLeftPress}
                            color={props.iconColor}
                            style={{ marginTop: 2 }}
                        />
                    )
                }
                titleStyle={[
                    props.length > 20 ? styles.smallTitle : styles.title,
                    { color: 'black' },
                    props.titleColor && { color: props.titleColor }
                ]}
                {...props}
            />
        </Block>
    );
}

const styles = StyleSheet.create({
    title: {
        fontSize: 21,
        fontWeight: 'bold',
    },
    smallTitle: {
        fontSize: 19,
        fontWeight: 'bold',
    },
    navbar: {
        paddingVertical: 0,
        paddingBottom: 30,
        paddingTop: 60,
        zIndex: 5,
    },
    shadow: {
        backgroundColor: theme.COLORS.WHITE,
        shadowColor: 'black',
        shadowOffset: { width: 0, height: 2 },
        shadowRadius: 6,
        shadowOpacity: 0.2,
        elevation: 3,
    },
    header: {
        backgroundColor: theme.COLORS.WHITE
    }
});

export default withNavigation(Header);