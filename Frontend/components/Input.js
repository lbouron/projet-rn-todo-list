import React from 'react';
import { StyleSheet, Platform } from 'react-native';
import { Input } from 'galio-framework';
import PropTypes from 'prop-types';

import { theme } from '../constants';

export default class ArInput extends React.Component {
  render() {
    const { shadowless, success, error, editable } = this.props;
    
    const inputStyles = [
      styles.input,
      editable === false && styles.disabled,
      !shadowless && styles.shadow,
      success && styles.success,
      error && styles.error,
      { ...this.props.style }
    ];

    return (
      <Input
        placeholder='Placeholder'
        placeholderTextColor={theme.COLORS.MUTED}
        style={inputStyles}
        color={theme.COLORS.HEADER}
        {...this.props}
      />
    );
  }
}

ArInput.defaultProps = {
  shadowless: false,
  success: false,
  error: false
};

ArInput.propTypes = {
  shadowless: PropTypes.bool,
  success: PropTypes.bool,
  error: PropTypes.bool
}

const styles = StyleSheet.create({
  input: {
    ...Platform.select({
      ios: {
        height: 55
      },
      android: {
        height: 50
      }
    }),
    borderRadius: 10,
    borderColor: theme.COLORS.BORDER,
    backgroundColor: '#FFFFFF'
  },
  success: {
    borderColor: theme.COLORS.INPUT_SUCCESS,
  },
  error: {
    borderColor: theme.COLORS.INPUT_ERROR,
  },
  shadow: {
    shadowColor: theme.COLORS.BLACK,
    shadowOffset: { width: 0, height: 0.5 },
    shadowRadius: 1,
    shadowOpacity: 0.13,
    elevation: 2,
  },
  disabled: {
    backgroundColor: theme.COLORS.DISABLED
  }
});
