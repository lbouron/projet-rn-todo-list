import React from 'react';
import { Block, Text } from 'galio-framework';

import Button from './Button';
import Icon from './Icon';
import { fontsize } from '../constants';
import { styles } from '../assets/css/styles';

export default class SubCard extends React.Component {
    render() {
        const { task, detailsTask, editTask, deleteTask } = this.props;

        return (
            <Button style={styles.taches} onPress={detailsTask}>
                <Block row style={{ width: '100%', alignItems: 'center', justifyContent: 'space-between' }}>
                    <Text size={fontsize.body}>
                        {task.title}
                    </Text>

                    <Block row width={50} style={{ justifyContent: 'space-around', alignItems: 'center' }}>
                        <Icon
                            name='edit'
                            family='font-awesome'
                            size={fontsize.body}
                            onPress={editTask}
                        />

                        <Icon
                            name='trash-o'
                            family='font-awesome'
                            size={fontsize.body}
                            onPress={deleteTask}
                        />
                    </Block>
                </Block>
            </Button>
        );
    }
}