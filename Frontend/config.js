import * as app from './app.json';

let config = module.exports;

config.app = {
    name: app.expo.name
}

config.serverApi = {
    route: 'http://{adresseIP}:3000/api'
    // Exemple : 'http://45.45.45.45:3000/api'
}