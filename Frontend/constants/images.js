// Images Projet Todo List

// StartScreen
const ManageTasksWoman = require('../assets/imgs/Managetasks-Woman.jpg');

// Register et Connexion
const SigninWoman = require('../assets/imgs/Signin-Woman.jpg');

// Home
const BoardPeople = require('../assets/imgs/Board-People.jpg');

// Profile
const ProfileWoman = require('../assets/imgs/Profile-Woman.jpg');

// Add et Edit
const CreateColumnWoman = require('../assets/imgs/CreateColumn-Woman.jpg');

const MultiTasksGuy = require('../assets/imgs/Multitasks-Guy.jpg');

export default {
  ManageTasksWoman,
  SigninWoman,
  ProfileWoman,
  BoardPeople,
  CreateColumnWoman,
  MultiTasksGuy
};