import React from 'react';

import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';

// Principaux screens
import StartScreen from '../screens/StartScreen';

// Authentification
import Register from '../screens/authentification/Register';
import Connexion from '../screens/authentification/Connexion';

// Tableaux de bord
import Dashboards from '../screens/dashboards/Dashboards';
import NewDashboard from '../screens/dashboards/NewDashboard';
import EditDashboard from '../screens/dashboards/EditDashboard';

// Colonnes
import Columns from '../screens/columns/Columns';
import NewColumn from '../screens/columns/NewColumn';
import EditColumn from '../screens/columns/EditColumn';

// Tâches
import DetailsTask from '../screens/tasks/DetailsTask';
import NewTask from '../screens/tasks/NewTask';
import EditTask from '../screens/tasks/EditTask';

// Profil
import Profile from '../screens/profile/Profile';

// Components pour les screens
import { Icon, Header } from '../components';
import { fontsize, theme } from '../constants';

const Stack = createStackNavigator();
const Tab = createBottomTabNavigator();

function AppStack(props) {
    return (
        <Tab.Navigator
            screenOptions={({ route }) => ({
                gestureEnabled: false,
                tabBarActiveTintColor: theme.COLORS.PRIMARY,
                tabBarInactiveTintColor: theme.COLORS.MUTED,
                tabBarStyle: {
                    height: 55,
                    backgroundColor: theme.COLORS.WHITE,
                    paddingTop: 5,
                    paddingBottom: 5
                },
            })}
        >
            <Tab.Screen
                name='Dashboards'
                component={TasksStack}
                options={{
                    title: 'Tableaux de bord',
                    header: () => null,
                    tabBarIcon: ({ color }) => (
                        <Icon
                            name='home'
                            family='font-awesome'
                            color={color}
                            size={28}
                        />
                    ),
                }}
            />

            <Tab.Screen
                name='Profil'
                component={ProfileStack}
                options={{
                    title: 'Profil',
                    header: () => null,
                    tabBarIcon: ({ color }) => (
                        <Icon
                            name='user'
                            family='font-awesome'
                            color={color}
                            size={28}
                        />
                    ),
                }}
            />
        </Tab.Navigator>
    );
}

function TasksStack(props) {
    return (
        <Stack.Navigator initialRouteName='TasksStack_Dashboards'>
            <Stack.Screen
                name='TasksStack_Dashboards'
                component={Dashboards}
                options={{
                    header: ({ navigation, scene }) => (
                        <Header
                            title='Tableaux de bord'
                            navigation={navigation}
                            scene={scene}
                        />
                    ),
                }}
            />

            <Stack.Screen
                name='TasksStack_NewDashboard'
                component={NewDashboard}
                options={{
                    header: ({ navigation, scene }) => (
                        <Header
                            back
                            title='Nouveau tableau'
                            navigation={navigation}
                            scene={scene}
                        />
                    ),
                }}
            />

            <Stack.Screen
                name='TasksStack_EditDashboard'
                component={EditDashboard}
                options={{
                    header: ({ navigation, scene }) => (
                        <Header
                            back
                            title='Modifier le tableau'
                            navigation={navigation}
                            scene={scene}
                        />
                    ),
                }}
            />

            <Stack.Screen
                name='TasksStack_Columns'
                component={Columns}
                options={{
                    header: ({ navigation, scene }) => (
                        <Header
                            back
                            title='Groupes de tâches'
                            navigation={navigation}
                            scene={scene}
                        />
                    ),
                }}
            />

            <Stack.Screen
                name='TasksStack_NewColumn'
                component={NewColumn}
                options={{
                    header: ({ navigation, scene }) => (
                        <Header
                            back
                            title='Nouvelle colonne'
                            navigation={navigation}
                            scene={scene}
                        />
                    ),
                }}
            />

            <Stack.Screen
                name='TasksStack_EditColumn'
                component={EditColumn}
                options={{
                    header: ({ navigation, scene }) => (
                        <Header
                            back
                            title='Modifier la colonne'
                            navigation={navigation}
                            scene={scene}
                        />
                    ),
                }}
            />

            <Stack.Screen
                name='TasksStack_DetailsTask'
                component={DetailsTask}
                options={{
                    header: ({ navigation, scene }) => (
                        <Header
                            back
                            title='Tâches'
                            navigation={navigation}
                            scene={scene}
                        />
                    ),
                }}
            />

            <Stack.Screen
                name='TasksStack_NewTask'
                component={NewTask}
                options={{
                    header: ({ navigation, scene }) => (
                        <Header
                            back
                            title='Nouvelle tâche'
                            navigation={navigation}
                            scene={scene}
                        />
                    ),
                }}
            />

            <Stack.Screen
                name='TasksStack_EditTask'
                component={EditTask}
                options={{
                    header: ({ navigation, scene }) => (
                        <Header
                            back
                            title='Modifier la tâche'
                            navigation={navigation}
                            scene={scene}
                        />
                    )
                }}
            />
        </Stack.Navigator>
    )
}

function ProfileStack(props) {
    return (
        <Stack.Navigator initialRouteName='ProfileStack_Profile'>
            <Stack.Screen
                name='ProfileStack_Profile'
                component={Profile}
                options={{
                    header: ({ navigation, scene }) => (
                        <Header
                            title='Profil'
                            navigation={navigation}
                            scene={scene}
                        />
                    ),
                }}
            />
        </Stack.Navigator>
    );
}

function AuthenticationStack(props) {
    return (
        <Stack.Navigator initialRouteName='RegisterStack_Register'>
            <Stack.Screen
                name='Connexion'
                component={Connexion}
                options={{ headerShown: false }}
            />

            <Stack.Screen
                name='Register'
                component={Register}
                options={{
                    headerShown: true,
                    header: ({ navigation, scene }) => (
                        <Header
                            back
                            noShadow
                            navigation={navigation}
                            scene={scene}
                        />
                    ),
                }}
            />
        </Stack.Navigator>
    );
}

export default function OnboardingStack(props) {
    return (
        <Stack.Navigator screenOptions={{ headerShown: false, presentation: 'card' }}>
            <Stack.Group>
                <Stack.Screen name='StartScreen' component={StartScreen} />
                <Stack.Screen name='AppStack' component={AppStack} />
                <Stack.Screen name='AuthenticationStack' component={AuthenticationStack} />
            </Stack.Group>
        </Stack.Navigator>
    );
}