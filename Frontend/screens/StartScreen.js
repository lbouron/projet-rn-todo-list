import React, { createRef } from 'react';
import {
    StyleSheet,
    Dimensions,
    Image
} from 'react-native';
import { Block, Text } from 'galio-framework';
import AsyncStorage from '@react-native-async-storage/async-storage';

import { images, theme, storage } from '../constants';

import { appLogger, apiLogger } from '../services/logger';
import Authentication from '../authentication/Authentication';
import config from '../config';

const { width, height } = Dimensions.get('screen');

export default class StartScreen extends React.Component {
    async componentDidMount() {
        await this.tryAutoSignIn();
    }

    async tryAutoSignIn() {
        const { navigation } = this.props;

        const email = await AsyncStorage.getItem(storage.UserEmail);
        const password = await AsyncStorage.getItem(storage.UserPassword);

        if (email && password) {
            // Essai connexion auto avec identifiants email / mot de passe
            appLogger.info('Tentative de reconnexion avec email / mot de passe.');

            if (await Authentication.Login(email, password, false)) {
                appLogger.info('Connexion automatique par email / mot de passe réussie.');
                navigation.navigate('AppStack');
            } else {
                appLogger.warn('Connexion automatique par email / mot de passe échouée.');
                navigation.navigate('AuthenticationStack');
            }
        } else {
            appLogger.info('Aucun identifiant de connexion précédente trouvé.');
            navigation.navigate('AuthenticationStack');
        }
    }

    render() {
        return (
            <Block safe flex style={styles.container}>
                <Image
                    source={images.ManageTasksWoman}
                    style={styles.banniere}
                    resizeMode='contain'
                />

                <Text h1 bold center style={{ textTransform: 'uppercase' }}>
                    Todo List
                </Text>
            </Block>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        width: width,
        height: height,
        backgroundColor: theme.COLORS.WHITE,
        justifyContent: 'center',
        alignItems: 'center'
    },
    banniere: {
        width: width,
        height: height / 2
    }
});