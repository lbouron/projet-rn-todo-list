import React from 'react';
import {
    Dimensions,
    TouchableWithoutFeedback,
    Keyboard,
    KeyboardAvoidingView
} from 'react-native';
import { Block, Text } from 'galio-framework';

import { Banner, AuthSocial, Button, Input } from '../../components';
import { images, theme, fontsize } from '../../constants';
import { styles } from '../../assets/css/styles';

import Authentication from '../../authentication/Authentication';

const { width, height } = Dimensions.get('screen');

export default class Connexion extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            email: null,
            password: null,
            apiError: false
        }
    }

    render() {
        const { navigation } = this.props;

        return (
            <KeyboardAvoidingView behavior='position' style={{ ...styles.connexion, paddingTop: 40 }}>
                <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
                    <Block center>
                        {/* Bannière */}
                        <Banner images={images.SigninWoman} />

                        <Block style={{ height: height / 1.7, justifyContent: 'space-around' }}>
                            <Block>
                                {/* Titre */}
                                <Text h1 bold center style={{ textTransform: 'uppercase' }}>
                                    Todo List
                                </Text>

                                {/* Inputs de l'email et du mot de passe */}
                                <Block row style={{ alignItems: 'center' }}>
                                    <Text
                                        size={fontsize.body}
                                        style={{
                                            position: 'absolute',
                                            zIndex: 2,
                                            marginLeft: 15
                                        }}
                                    >
                                        Email
                                    </Text>

                                    <Input
                                        shadowless
                                        keyboardType='email-address'
                                        autoCapitalize='none'
                                        placeholder='Email'
                                        placeholderTextColor={theme.COLORS.HEADER}
                                        style={{
                                            ...styles.input,
                                            borderTopLeftRadius: 10,
                                            borderTopRightRadius: 10
                                        }}
                                        onChangeText={email => this.setState({ email })}
                                    />
                                </Block>

                                <Block row style={{ marginTop: -18, alignItems: 'center' }}>
                                    <Text
                                        size={fontsize.body}
                                        style={{
                                            position: 'absolute',
                                            zIndex: 2,
                                            marginLeft: 15
                                        }}
                                    >
                                        Mot de passe
                                    </Text>

                                    <Input
                                        shadowless
                                        password
                                        secureTextEntry={true}
                                        autoCapitalize='none'
                                        placeholder='Mot de passe'
                                        placeholderTextColor={theme.COLORS.HEADER}
                                        style={{
                                            ...styles.input,
                                            borderBottomLeftRadius: 10,
                                            borderBottomRightRadius: 10
                                        }}
                                        onChangeText={password => this.setState({ password })}
                                    />
                                </Block>
                            </Block>

                            <Block center>
                                <AuthSocial />
                            </Block>

                            {/* Block Texte Register & Bouton Connexion */}
                            <Block center>
                                <Text size={fontsize.body}>
                                    Pas encore de compte ? Inscrivez-vous <Text bold color={theme.COLORS.PRIMARY} onPress={() => navigation.navigate('Register')}>ici</Text>.
                                </Text>

                                <Button shadowless onPress={() => this.login()} style={styles.formbutton}>
                                    <Text size={fontsize.body} color={theme.COLORS.WHITE}>
                                        Connexion
                                    </Text>
                                </Button>
                            </Block>
                        </Block>
                    </Block>
                </TouchableWithoutFeedback>
            </KeyboardAvoidingView>
        );
    }

    // Fonction qui permet de se connecter
    login = async () => {
        const { navigation } = this.props;
        
        if (await Authentication.Login(this.state.email, this.state.password)) {
            navigation.navigate('AppStack');
        } else {
            this.setState({ apiError: true });
        }
    }
}