import React from 'react';
import {
    Dimensions,
    TouchableWithoutFeedback,
    Keyboard,
    KeyboardAvoidingView,
    Alert
} from 'react-native';
import { Block, Text } from 'galio-framework';

import { Banner, Button, Input } from '../../components';
import { images, theme, fontsize } from '../../constants';
import { styles } from '../../assets/css/styles';

import Authentication from '../../authentication/Authentication';
import config from '../../config';

const { width, height } = Dimensions.get('screen');

export default class Register extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            name: null,
            email: null,
            password: null,
            passwordCheck: null,
            apiRequest: false
        }
    }

    render() {
        return (
            <KeyboardAvoidingView behavior='position' style={styles.connexion}>
                <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
                    <Block center>
                        {/* Bannière */}
                        <Banner images={images.SigninWoman} />

                        <Block style={{ height: height / 2, justifyContent: 'space-around' }}>
                            <Block>
                                {/* Titre */}
                                <Text h1 bold center>
                                    Inscription
                                </Text>

                                {/* Inputs de l'email, du mot de passe et de sa confirmation */}
                                <Block row style={{ alignItems: 'center' }}>
                                    <Text
                                        size={fontsize.body}
                                        style={{
                                            position: 'absolute',
                                            zIndex: 2,
                                            marginLeft: 15
                                        }}
                                    >
                                        Email
                                    </Text>

                                    <Input
                                        shadowless
                                        keyboardType='email-address'
                                        autoCapitalize='none'
                                        placeholder='Email'
                                        placeholderTextColor={theme.COLORS.HEADER}
                                        style={{
                                            ...styles.input,
                                            borderTopLeftRadius: 10,
                                            borderTopRightRadius: 10
                                        }}
                                        onChangeText={email => this.setState({ email })}
                                    />
                                </Block>

                                <Block row style={{ marginTop: -18, alignItems: 'center' }}>
                                    <Text
                                        size={fontsize.body}
                                        style={{
                                            position: 'absolute',
                                            zIndex: 2,
                                            marginLeft: 15
                                        }}
                                    >
                                        Mot de passe
                                    </Text>
                                    
                                    <Input
                                        shadowless
                                        password
                                        secureTextEntry={true}
                                        autoCapitalize='none'
                                        placeholder='Mot de passe'
                                        placeholderTextColor={theme.COLORS.HEADER}
                                        style={{
                                            ...styles.input
                                        }}
                                        onChangeText={password => this.setState({ password })}
                                    />
                                </Block>

                                <Block row style={{ marginTop: -18, alignItems: 'center' }}>
                                    <Text
                                        size={fontsize.body}
                                        style={{
                                            position: 'absolute',
                                            zIndex: 2,
                                            marginLeft: 15
                                        }}
                                    >
                                        {`Confirmer
mot de passe`}
                                    </Text>
                                    
                                    <Input
                                        shadowless
                                        password
                                        secureTextEntry={true}
                                        autoCapitalize='none'
                                        placeholder='Confirmation mot de passe'
                                        placeholderTextColor={theme.COLORS.HEADER}
                                        style={{
                                            ...styles.input,
                                            borderBottomLeftRadius: 10,
                                            borderBottomRightRadius: 10
                                        }}
                                        onChangeText={passwordCheck => this.setState({ passwordCheck })}
                                    />
                                </Block>
                            </Block>

                            {/* Bouton Inscription */}
                            <Button shadowless onPress={() => this.register()} style={styles.formbutton}>
                                <Text size={fontsize.body} color={theme.COLORS.WHITE}>
                                    Inscription
                                </Text>
                            </Button>
                        </Block>
                    </Block>
                </TouchableWithoutFeedback>
            </KeyboardAvoidingView>
        );
    }

    // Fonction qui permet de s'inscrire
    register = async () => {
        const { navigation } = this.props;

        if (this.state.email === null || this.state.email.length < 2) {
            Alert.alert(config.app.name, 'L\'email doit faire deux caractères minimum et contenir un @.');
        } else if (this.state.password === null || this.state.password.length < 8) {
            Alert.alert(config.app.name, 'Le mot de passe doit faire huit caractères minimum.');
        } else if (this.state.passwordCheck === null || this.state.passwordCheck !== this.state.password) {
            Alert.alert(config.app.name, 'La confirmation du mot de passe doit être identique au mot de passe.');
        } else {
            const data = {
                email: this.state.email,
                password: this.state.password
            }
    
            if (await Authentication.Register(data, navigation)) {
                if (await Authentication.Login(data.email, data.password, false)) {
                    navigation.navigate('AppStack');
                }
            }
        }
    }
}