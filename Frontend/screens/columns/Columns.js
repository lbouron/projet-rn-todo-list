import React from 'react';
import { ScrollView, Alert } from 'react-native';
import { Block, Text } from 'galio-framework';

import { Button, Icon, Accordion, SubCard, Banner } from '../../components';
import { images, theme, fontsize } from '../../constants';
import { styles } from '../../assets/css/styles';

import todolistApiService from '../../services/todolistApiService';
import Utils from '../../utils/Utils';
import config from '../../config';
import { apiLogger } from '../../services/logger';

export default class Columns extends React.Component {
    constructor(props) {
        super(props);

        this.navigation = props.navigation;

        this.state = {
            listTasks: [],
            loading: false
        }
    }

    async componentDidMount() {
        // Appel de la fonction getColumnsByDashboardWithTasks au lancement de la page
        this.focusListener = this.navigation.addListener('focus', async () => {
            await this.getColumnsByDashboardWithTasks();
        });
    }

    // Fonction qui récupère toutes les colonnes (et leurs tâches) d'un tableau de bord
    getColumnsByDashboardWithTasks = async () => {
        try {
            const { route } = this.props;
            const detailsDashboard = route.params.item;

            this.setState({ loading: true });
            let listTasks = await Utils.getColumnsByDashboardWithTasks(detailsDashboard.id);

            if (listTasks !== null) {
                this.setState({ listTasks: listTasks });
            }
        } catch (error) { }
        this.setState({ loading: false });
    }

    render() {
        const { navigation, isFocused, route } = this.props;
        const detailsDashboard = route.params.item;

        return (
            <Block safe flex style={styles.container}>
                {/* En-tête */}
                <Block row style={{ marginHorizontal: 15, justifyContent: 'space-between', alignItems: 'flex-start' }}>
                    {/* Titre du tableau de bord ouvert */}
                    <Text h1 size={fontsize.title1} style={{ width: '60%' }}>
                        {detailsDashboard.title}
                    </Text>

                    {/* Bouton Ajout */}
                    <Button shadowless style={styles.button} onPress={() => navigation.navigate('TasksStack_NewColumn', { item: detailsDashboard.id })}>
                        <Block row style={{ alignItems: 'center' }}>
                            <Icon
                                name='plus'
                                family='font-awesome'
                                color={theme.COLORS.WHITE}
                                size={fontsize.body}
                            />
                            
                            <Text size={fontsize.body} color={theme.COLORS.WHITE} style={{ marginLeft: 5 }}>
                                Nouvelle colonne
                            </Text>
                        </Block>
                    </Button>
                </Block>

                {/* Bannière */}
                <Banner images={images.BoardPeople} />

                {/* Liste des colonnes avec les tâches */}
                <ScrollView showsVerticalScrollIndicator={false}>
                    <Block style={{ paddingVertical: 10 }}>
                        {this.state.listTasks.map((column, index) => (
                            <Accordion
                                key={index}
                                column={column}
                                createTask={() => navigation.navigate('TasksStack_NewTask', { item: column.id })}
                                editColumn={() => navigation.navigate('TasksStack_EditColumn', { item: column })}
                                deleteColumn={() => Alert.alert(config.app.name, 'Voulez-vous vraiment supprimer cette colonne ?',
                                                    [
                                                        { text: 'Supprimer', onPress: () => this.deleteColumn(column.id)},
                                                        { text: 'Annuler', style: 'cancel' }
                                                    ])}
                                titleTasks={(column.tasks.map((task, index) => (
                                    <SubCard
                                        key={index}
                                        task={task}
                                        detailsTask={() => navigation.navigate('TasksStack_DetailsTask', { item: task })}
                                        editTask={() => navigation.navigate('TasksStack_EditTask', { item: task, dashboard: detailsDashboard })}
                                        deleteTask={() => Alert.alert(config.app.name, 'Voulez-vous vraiment supprimer cette tâche ?',
                                                            [
                                                                { text: 'Supprimer', onPress: () => this.deleteTask(task.id)},
                                                                { text: 'Annuler', style: 'cancel' }
                                                            ])}
                                    />)))}
                            />
                        ))}
                    </Block>
                </ScrollView>
            </Block>
        );
    }

    // Fonction qui supprime une colonne et ses tâches via l'id de la colonne
    deleteColumn = async (id) => {
        try {
            const URL = `/columns/delete/${id}`;

            let response = await todolistApiService.request({
                url: URL,
                method: 'DELETE'
            });

            if (response.data.result) {
                apiLogger.success('La colonne a bien été supprimée.');
            }
        } catch (error) {
            Alert.alert(config.app.name, 'Il y a eu une erreur lors de la suppression de la colonne.');
            apiLogger.error('Erreur lors de la suppression de la colonne.');
            apiLogger.error(error);
        }
    }

    // Fonction qui supprime une tâche via son id
    deleteTask = async (id) => {
        try {
            const URL = `/tasks/delete/${id}`;

            let response = await todolistApiService.request({
                url: URL,
                method: 'DELETE'
            });

            if (response.data.result) {
                apiLogger.success('La tâche a bien été supprimée.');
            }
        } catch (error) {
            Alert.alert(config.app.name, 'Il y a eu une erreur lors de la suppression de la tâche.');
            apiLogger.error('Erreur lors de la suppression de la tâche.');
            apiLogger.error(error);
        }
    }
}