import React from 'react';
import {
    StyleSheet,
    Dimensions,
    KeyboardAvoidingView,
    TouchableWithoutFeedback,
    Keyboard,
    Image,
    Alert
} from 'react-native';
import { Block, Text } from 'galio-framework';

import { Input, Button } from '../../components';
import { theme, images, fontsize } from '../../constants';
import { styles } from '../../assets/css/styles';

import todolistApiService from '../../services/todolistApiService';
import { apiLogger, appLogger } from '../../services/logger';
import config from '../../config';

const { width, height } = Dimensions.get('screen');

export default class EditColumn extends React.Component {
    constructor(props) {
        super(props);

        this.route = props.route;

        this.state = {
            updatedColumn: {
                title: this.route.params.item.title,
                columnId: this.route.params.item.id
            },
            isError: {
                title: null
            },
            dashboardId: this.route.params.item.dashboardId
        };
    }

    render() {
        return (
            <KeyboardAvoidingView behavior='position' style={styles.container}>
                <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
                    <Block center>
                        {/* Bannière */}
                        <Image
                            source={images.CreateColumnWoman}
                            style={styles.banniere}
                            resizeMode='contain'
                        />

                        {/* Input du titre */}
                        <Block style={{ height: height / 2.5, justifyContent: 'space-around' }}>
                            <Block row style={{ alignItems: 'center' }}>
                                <Text
                                    size={fontsize.body}
                                    style={{
                                        position: 'absolute',
                                        zIndex: 2,
                                        marginLeft: 15
                                    }}
                                >
                                    Titre
                                </Text>

                                <Input
                                    shadowless
                                    placeholder='Titre'
                                    placeholderTextColor={theme.COLORS.HEADER}
                                    style={{
                                        ...styles.input,
                                        borderRadius: 10
                                    }}
                                    value={this.state.updatedColumn.title}
                                    onChangeText={text => this.setState({
                                        updatedColumn: {
                                            ...this.state.updatedColumn,
                                            title: text
                                        }
                                    })}
                                    error={this.state.isError.title}
                                />
                            </Block>
                            {this.state.isError.title && <Text color={theme.COLORS.ERROR}>
                                Veuillez saisir un titre.
                            </Text>}

                            {/* Bouton Valider */}
                            <Button shadowless onPress={() => this.updateColumn()} style={styles.formbutton}>
                                <Text size={fontsize.body} color={theme.COLORS.WHITE}>
                                    Modifier
                                </Text>
                            </Button>
                        </Block>
                    </Block>
                </TouchableWithoutFeedback>
            </KeyboardAvoidingView>
        );
    }

    // Fonction qui modifie le titre d'une colonne
    updateColumn = async () => {
        try {
            const { navigation } = this.props;
            const URL = `/columns/update/${this.state.updatedColumn.columnId}`;
            
            if (this.state.updatedColumn.title !== null) {
                let data = {
                    title: this.state.updatedColumn.title
                }

                let response = await todolistApiService.request({
                    url: URL,
                    method: 'POST',
                    data: data
                });

                if (response.data.result) {
                    apiLogger.success('La colonne a bien été modifiée.');
                    navigation.navigate('TasksStack_Columns', { item: this.state.dashboardId });
                }
            } else {
                Alert.alert(config.app.name, 'Vous devez renseigner un titre.');
                appLogger.error('Erreur lors de la modification de la colonne.');
            }
        } catch (error) {
            Alert.alert(config.app.name, 'Il y a eu une erreur lors de la modification de la colonne.');
            apiLogger.error('Erreur lors de la modification de la colonne.');
            apiLogger.error(error);
        }
    }
}