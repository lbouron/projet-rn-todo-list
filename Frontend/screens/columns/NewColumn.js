import React from 'react';
import {
    StyleSheet,
    Dimensions,
    KeyboardAvoidingView,
    TouchableWithoutFeedback,
    Keyboard,
    Image,
    Alert
} from 'react-native';
import { Block, Text } from 'galio-framework';

import { Input, Button } from '../../components';
import { theme, images, fontsize } from '../../constants';
import { styles } from '../../assets/css/styles';

import todolistApiService from '../../services/todolistApiService';
import { apiLogger, appLogger } from '../../services/logger';
import config from '../../config';

const { width, height } = Dimensions.get('screen');

export default class NewColumn extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            title: null,
            apiRequest: false
        };
    }

    render() {
        return (
            <KeyboardAvoidingView behavior='position' style={styles.container}>
                <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
                    <Block center>
                        {/* Bannière */}
                        <Image
                            source={images.CreateColumnWoman}
                            style={styles.banniere}
                            resizeMode='contain'
                        />

                        {/* Input du titre */}
                        <Block style={{ height: height / 2.5, justifyContent: 'space-around' }}>
                            <Block row style={{ alignItems: 'center' }}>
                                <Text
                                    size={fontsize.body}
                                    style={{
                                        position: 'absolute',
                                        zIndex: 2,
                                        marginLeft: 15
                                    }}
                                >
                                    Titre
                                </Text>

                                <Input
                                    shadowless
                                    placeholder='Titre'
                                    placeholderTextColor={theme.COLORS.HEADER}
                                    style={{
                                        ...styles.input,
                                        borderRadius: 10
                                    }}
                                    onChangeText={title => this.setState({ title })}
                                />
                            </Block>

                            {/* Bouton Valider */}
                            <Button shadowless onPress={() => this.createColumn()} style={styles.formbutton}>
                                <Text size={fontsize.body} color={theme.COLORS.WHITE}>
                                    Valider
                                </Text>
                            </Button>
                        </Block>
                    </Block>
                </TouchableWithoutFeedback>
            </KeyboardAvoidingView>
        );
    }

    // Fonction qui crée une colonne
    createColumn = async () => {
        try {
            const { navigation, route } = this.props;
            const detailsDashboard = route.params.item;
            const URL = `/columns/add`;
            
            if (this.state.title !== null) {
                let data = {
                    title: this.state.title,
                    dashboardId: detailsDashboard
                }

                let response = await todolistApiService.request({
                    url: URL,
                    method: 'PUT',
                    data: data
                });

                if (response.data.result) {
                    apiLogger.success('La colonne a bien été créée.');
                    navigation.navigate('TasksStack_Columns', { item: detailsDashboard });
                }
            } else {
                Alert.alert(config.app.name, 'Vous devez renseigner un titre.');
                appLogger.error('Erreur lors de la création de la colonne.');
            }
        } catch (error) {
            Alert.alert(config.app.name, 'Il y a eu une erreur lors de la création de la colonne.');
            apiLogger.error('Erreur lors de la création de la colonne.');
            apiLogger.error(error);
        }
    }
}