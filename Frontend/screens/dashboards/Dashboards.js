import React from 'react';
import { ScrollView, Alert } from 'react-native';
import { Block, Text } from 'galio-framework';

import { Button, Icon, Card, Banner } from '../../components';
import { images, theme, fontsize } from '../../constants';
import { styles } from '../../assets/css/styles';

import todolistApiService from '../../services/todolistApiService';
import Utils from '../../utils/Utils';
import config from '../../config';
import { apiLogger } from '../../services/logger';

export default class Dashboards extends React.Component {
    constructor(props) {
        super(props);

        this.navigation = props.navigation;

        this.state = {
            listDashboards: [],
            loading: false
        }
    }

    async componentDidMount() {
        // Appel de la fonction getDashboardsByUser au lancement de la page
        this.focusListener = this.navigation.addListener('focus', async () => {
            await this.getDashboardsByUser();
        });
    }

    // Fonction qui récupère tous les tableaux de bord d'un utilisateur
    getDashboardsByUser = async () => {
        try {
            this.setState({ loading: true });
            let listDashboards = await Utils.getDashboardsByUser();

            if (listDashboards !== null) {
                this.setState({ listDashboards: listDashboards });
            }
        } catch (error) { }
        this.setState({ loading: false });
    }

    render() {
        const { navigation, isFocused } = this.props;

        return (
            <Block safe flex style={styles.container}>
                {/* En-tête */}
                <Block right style={{ marginHorizontal: 15 }}>
                    {/* Bouton Ajout */}
                    <Button shadowless style={styles.button} onPress={() => navigation.navigate('TasksStack_NewDashboard')}>
                        <Block row style={{ alignItems: 'center' }}>
                            <Icon
                                name='plus'
                                family='font-awesome'
                                color={theme.COLORS.WHITE}
                                size={fontsize.body}
                            />
                            
                            <Text size={fontsize.body} color={theme.COLORS.WHITE} style={{ marginLeft: 5 }}>
                                Nouveau tableau de bord
                            </Text>
                        </Block>
                    </Button>
                </Block>

                {/* Bannière */}
                <Banner images={images.BoardPeople} />
                
                {/* Liste des tableaux de bord */}
                <ScrollView showsVerticalScrollIndicator={false}>
                    <Block center style={{ paddingVertical: 10 }}>
                        {this.state.listDashboards.map((dashboard, index) => (
                            <Card
                                key={index}
                                dashboard={dashboard}
                                onPress={() => navigation.navigate('TasksStack_Columns', { item: dashboard })}
                                editDashboard={() => navigation.navigate('TasksStack_EditDashboard', { item: dashboard })}
                                deleteDashboard={() => Alert.alert(config.app.name, 'Voulez-vous vraiment supprimer ce tableau de bord ?',
                                                        [
                                                            { text: 'Supprimer', onPress: () => this.deleteDashboard(dashboard.id)},
                                                            { text: 'Annuler', style: 'cancel' }
                                                        ])}
                            />
                        ))}
                    </Block>
                </ScrollView>
            </Block>
        );
    }

    // Fonction qui supprime un tableau via son id
    deleteDashboard = async (id) => {
        try {
            const URL = `/dashboards/delete/${id}`;

            let response = await todolistApiService.request({
                url: URL,
                method: 'DELETE'
            });

            if (response.data.result) {
                apiLogger.success('Le tableau de bord a bien été supprimé.');
            }
        } catch (error) {
            Alert.alert(config.app.name, 'Il y a eu une erreur lors de la suppression du tableau de bord.');
            apiLogger.error('Erreur lors de la suppression du tableau de bord.');
            apiLogger.error(error);
        }
    }
}