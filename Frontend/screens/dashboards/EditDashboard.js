import React from 'react';
import {
    Dimensions,
    KeyboardAvoidingView,
    TouchableWithoutFeedback,
    Keyboard,
    Image,
    Alert
} from 'react-native';
import { Block, Text } from 'galio-framework';

import { Input, Button } from '../../components';
import { theme, images, fontsize } from '../../constants';
import { styles } from '../../assets/css/styles';

import todolistApiService from '../../services/todolistApiService';
import { apiLogger, appLogger } from '../../services/logger';
import config from '../../config';

const { width, height } = Dimensions.get('screen');

export default class EditDashboard extends React.Component {
    constructor(props) {
        super(props);

        this.route = props.route;

        this.state = {
            updatedDashboard: {
                title: this.route.params.item.title,
                dashboardId: this.route.params.item.id
            },
            isError: {
                title: null
            }
        };
    }

    render() {
        return (
            <KeyboardAvoidingView behavior='position' style={styles.container}>
                <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
                    <Block center>
                        {/* Bannière */}
                        <Image
                            source={images.CreateColumnWoman}
                            style={styles.banniere}
                            resizeMode='contain'
                        />

                        {/* Input du titre */}
                        <Block style={{ height: height / 2.5, justifyContent: 'space-around' }}>
                            <Block row style={{ alignItems: 'center' }}>
                                <Text
                                    size={fontsize.body}
                                    style={{
                                        position: 'absolute',
                                        zIndex: 2,
                                        marginLeft: 15
                                    }}
                                >
                                    Titre
                                </Text>

                                <Input
                                    shadowless
                                    placeholder='Titre'
                                    placeholderTextColor={theme.COLORS.HEADER}
                                    style={{
                                        ...styles.input,
                                        borderRadius: 10
                                    }}
                                    value={this.state.updatedDashboard.title}
                                    onChangeText={text => this.setState({
                                        updatedDashboard: {
                                            ...this.state.updatedDashboard,
                                            title: text
                                        }
                                    })}
                                    error={this.state.isError.title}
                                />
                            </Block>
                            {this.state.isError.title && <Text color={theme.COLORS.ERROR}>
                                Veuillez saisir un titre.
                            </Text>}

                            {/* Bouton Valider */}
                            <Button shadowless onPress={() => this.updateDashboard()} style={styles.formbutton}>
                                <Text size={fontsize.body} color={theme.COLORS.WHITE}>
                                    Modifier
                                </Text>
                            </Button>
                        </Block>
                    </Block>
                </TouchableWithoutFeedback>
            </KeyboardAvoidingView>
        );
    }

    // Fonction qui modifie le titre d'un tableau
    updateDashboard = async () => {
        try {
            const { navigation } = this.props;
            const URL = `/dashboards/update/${this.state.updatedDashboard.dashboardId}`;
            
            if (this.state.updatedDashboard.title !== null) {
                let data = {
                    title: this.state.updatedDashboard.title
                }

                let response = await todolistApiService.request({
                    url: URL,
                    method: 'POST',
                    data: data
                });

                if (response.data.result) {
                    apiLogger.success('Le tableau de bord a bien été modifié.');
                    navigation.navigate('TasksStack_Dashboards');
                }
            } else {
                Alert.alert(config.app.name, 'Vous devez renseigner un titre.');
                appLogger.error('Erreur lors de la modification du tableau de bord.');
            }
        } catch (error) {
            Alert.alert(config.app.name, 'Il y a eu une erreur lors de la modification du tableau de bord.');
            apiLogger.error('Erreur lors de la modification du tableau de bord.');
            apiLogger.error(error);
        }
    }
}