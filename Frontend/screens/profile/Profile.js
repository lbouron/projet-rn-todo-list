import React from 'react';
import { Block, Text } from 'galio-framework';

import { Banner, Button } from '../../components';
import { images, theme, fontsize } from '../../constants';
import { styles } from '../../assets/css/styles';

import Authentication from '../../authentication/Authentication';
import connectedProfile from '../../authentication/ConnectedProfile';

export default class Profile extends React.Component {
    render() {
        return (
            <Block safe flex center style={{ ...styles.container }}>
                {/* Bannière */}
                <Banner images={images.ProfileWoman} />

                <Text center size={fontsize.body}>
                    Vous êtes actuellement connecté(e) avec {connectedProfile.email}.
                </Text>

                <Button shadowless style={{ marginTop: 40 }} onPress={() => this.logout()}>
                    <Text size={fontsize.body} color={theme.COLORS.WHITE}>
                        Déconnexion
                    </Text>
                </Button>
            </Block>
        );
    }

    logout = async () => {
        await Authentication.Logout();
        this.props.navigation.navigate('AuthenticationStack');
    }
}