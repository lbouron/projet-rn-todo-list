import React from 'react';
import {
    Dimensions,
    ScrollView,
    Image
} from 'react-native';
import { Block, Text } from 'galio-framework';
import moment from 'moment';
import 'moment/locale/fr';

import { Button, Icon } from '../../components';
import { images, theme, fontsize } from '../../constants';
import { styles } from '../../assets/css/styles';

import todolistApiService from '../../services/todolistApiService';
import config from '../../config';

const { width, height } = Dimensions.get('screen');

export default class DetailsTask extends React.Component {
    constructor(props) {
        super(props);

        this.route = props.route;

        this.state = {
            baseURL: config.serverApi.route,
            title: this.route.params.item.title,
            content: this.route.params.item.content,
            image: this.route.params.item.image,
            created: this.route.params.item.createdAt,
            taskId: this.route.params.item.id
        }
    }

    render() {
        return (
            <Block safe flex style={{ ...styles.container, paddingHorizontal: 20 }}>
                {/* Titre du tableau de bord ouvert */}
                <Text h1 size={fontsize.title1}>
                    {this.state.title}
                </Text>

                {this.state.image === true &&
                    <Block center>
                        <Image
                            source={{ uri: `${this.state.baseURL}/tasks/image/${this.state.taskId}` }}
                            style={{ ...styles.image, marginVertical: 10 }}
                            resizeMode='cover'
                        />
                    </Block>}

                <Block style={styles.content}>
                    <ScrollView showsVerticalScrollIndicator={false}>
                        <Block style={{ paddingVertical: 10 }}>
                            {this.state.content == 'null' ?
                                (<Text></Text>)
                            : this.state.content !== null &&
                                (<Text size={fontsize.body} style={{ textAlign: 'justify' }}>
                                    {this.state.content}
                                </Text>)}
                        </Block>
                    </ScrollView>
                </Block>

                <Text italic size={fontsize.body}>
                    Tâche créée le {moment(this.state.created).format('DD MMMM YYYY')} à {moment(this.state.created).format('LT')}.
                </Text>
            </Block>
        );
    }
}