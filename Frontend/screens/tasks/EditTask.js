import React from 'react';
import {
    Dimensions,
    KeyboardAvoidingView,
    TouchableWithoutFeedback,
    Keyboard,
    ScrollView,
    TouchableOpacity,
    Image,
    Modal,
    FlatList,
    Alert
} from 'react-native';
import { Block, Text } from 'galio-framework';
import * as ImagePicker from 'expo-image-picker';

import { Input, Button, Icon } from '../../components';
import { theme, fontsize } from '../../constants';
import { styles } from '../../assets/css/styles';

import todolistApiService from '../../services/todolistApiService';
import Utils from '../../utils/Utils';
import config from '../../config';
import { apiLogger, appLogger } from '../../services/logger';

const { width, height } = Dimensions.get('screen');

export default class EditTask extends React.Component {
    constructor(props) {
        super(props);

        this.route = props.route;

        this.state = {
            baseURL: config.serverApi.route,
            updatedTask: {
                title: this.route.params.item.title,
                content: this.route.params.item.content
            },
            isError: {
                title: null,
                content: null
            },
            image: this.route.params.item.image,
            newImage: null,
            nomImage: '',
            taskId: this.route.params.item.id,
            columnId: this.route.params.item.columnId,
            listColumns: [],
            modalVisibleListColumns: false,
            numColumn: 'Colonne n°'
        };
    }

    async componentDidMount() {
        this.getColumnsByDashboard();
    }

    // Fonction qui récupère toutes les colonnes d'un tableau de bord
    getColumnsByDashboard = async () => {
        try {
            const { route } = this.props;
            const detailsDashboard = route.params.dashboard;

            // this.setState({ loading: true });
            let listColumns = await Utils.getColumnsByDashboard(detailsDashboard.id);

            if (listTasks !== null) {
                this.setState({ listColumns: listColumns });
            }
        } catch (error) { }
    }

    // Fonction pour sélectionner une image dans la galerie du téléphone
    showImagePicker = async () => {
        // Demande les permissions pour ouvrir la galerie d'images
        const permissions = await ImagePicker.requestMediaLibraryPermissionsAsync();

        // Accès refusé
        if (permissions.granted === false) {
            alert('L\'accès à la galerie a été refusé.');
            return;
        }

        // Accès autorisé, ouvre la galerie d'images
        let picker = await ImagePicker.launchImageLibraryAsync({
            mediaTypes: ImagePicker.MediaTypeOptions.Images,
            // allowsEditing: true
        });

        // Envoi de l'image sélectionnée dans le state
        if (!picker.cancelled) {
            let nomUri = picker.uri.split('/').pop();
            this.setState({
                newImage: picker.uri,
                nomImage: nomUri
            });
        }
    }

    deleteImage = async () => {
        this.setState({ 
            image: false,
            nomImage: null
        });
    }

    modalVisibleListColumns = (visible) => {
        this.setState({ modalVisibleListColumns: visible });
    }

    render() {
        return (
            // <ScrollView showsVerticalScrollIndicator={false}>
                <Block style={styles.container}>
                    <KeyboardAvoidingView behavior='position'>
                        <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
                            <Block center>
                                {/* Sélection de l'image dans la galerie */}
                                <TouchableOpacity onPress={() => this.showImagePicker()}>
                                    <Block style={{ ...styles.image, alignItems: 'center', justifyContent: 'center' }}>
                                        {this.state.image === true ?
                                            (<Block>
                                                <Image
                                                    source={{ uri: `${this.state.baseURL}/tasks/image/${this.state.taskId}` }}
                                                    style={styles.image}
                                                    resizeMode='cover'
                                                />

                                                <Icon
                                                    name='trash'
                                                    family='font-awesome'
                                                    size={40}
                                                    color={theme.COLORS.PRIMARY}
                                                    style={{
                                                        position: 'absolute',
                                                        marginLeft: '85%',
                                                        marginTop: '5%'
                                                    }}
                                                    onPress={() => this.deleteImage()}
                                                />
                                            </Block>)
                                            : this.state.newImage !== null ?
                                            (<Block>
                                                <Image
                                                    source={{ uri: this.state.newImage }}
                                                    style={styles.image}
                                                    resizeMode='cover'
                                                />

                                                <Icon
                                                    name='trash'
                                                    family='font-awesome'
                                                    size={40}
                                                    color={theme.COLORS.PRIMARY}
                                                    style={{
                                                        position: 'absolute',
                                                        marginLeft: '85%',
                                                        marginTop: '5%'
                                                    }}
                                                    onPress={() => this.deleteImage()}
                                                />
                                            </Block>)
                                            :
                                            (<Icon
                                                name='plus-circle'
                                                family='font-awesome'
                                                size={40}
                                                color={theme.COLORS.PRIMARY}
                                            />)
                                        }
                                    </Block>
                                </TouchableOpacity>

                                <Block style={{ height: height / 2.5, justifyContent: 'space-between' }}>
                                    <Block>
                                        <Block row style={{ alignItems: 'center' }}>
                                            <Text
                                                size={fontsize.body}
                                                style={{
                                                    position: 'absolute',
                                                    zIndex: 2,
                                                    marginLeft: 15
                                                }}
                                            >
                                                Titre
                                            </Text>

                                            <Input
                                                shadowless
                                                placeholder='Titre'
                                                placeholderTextColor={theme.COLORS.HEADER}
                                                style={{
                                                    ...styles.input,
                                                    borderTopLeftRadius: 10,
                                                    borderTopRightRadius: 10
                                                }}
                                                value={this.state.updatedTask.title}
                                                onChangeText={text => this.setState({
                                                    updatedTask: {
                                                        ...this.state.updatedTask,
                                                        title: text
                                                    }
                                                })}
                                                error={this.state.isError.title}
                                            />
                                        </Block>

                                        <Block row style={{ marginTop: -18, alignItems: 'flex-start' }}>
                                            <Text
                                                size={fontsize.body}
                                                style={{
                                                    position: 'absolute',
                                                    marginLeft: 15,
                                                    marginTop: 24,
                                                    zIndex: 2
                                                }}
                                            >
                                                Description
                                            </Text>

                                            <Input
                                                shadowless
                                                placeholder='Description de la tâche'
                                                placeholderTextColor={theme.COLORS.HEADER}
                                                multiline={true}
                                                style={{
                                                    ...styles.input,
                                                    height: 190,
                                                    paddingVertical: 10,
                                                    alignItems: 'flex-start',
                                                    borderBottomLeftRadius: 10,
                                                    borderBottomRightRadius: 10
                                                }}
                                                value={this.state.updatedTask.content}
                                                onChangeText={text => this.setState({
                                                    updatedTask: {
                                                        ...this.state.updatedTask,
                                                        content: text
                                                    }
                                                })}
                                            />
                                        </Block>
                                        {this.state.isError.title && <Text color={theme.COLORS.ERROR}>
                                            Veuillez saisir un titre.
                                        </Text>}
                                    </Block>

                                    {/* Bouton pour déplacer la tâche dans une autre colonne */}
                                    {/* <Button shadowless style={{ ...styles.formbutton, flexDirection: 'row', alignItems: 'center', justifyContent: 'flex-end' }}
                                        onPress={() => {
                                            this.getColumnsByDashboard(),
                                            this.modalVisibleListColumns(!this.state.modalVisibleListColumns)}
                                        }
                                    >
                                        <Input
                                            editable={false}
                                            borderless
                                            shadowless
                                            placeholder={this.state.numColumn}
                                            placeholderTextColor={theme.COLORS.WHITE}
                                            onChangeText={text => this.setState({ numColumn: text })}
                                            style={{ backgroundColor: 'transparent' }}
                                        />

                                        <Icon
                                            name='chevron-down'
                                            family='font-awesome'
                                            color={theme.COLORS.MUTED}
                                            size={25}
                                            style={{ marginRight: 10 }}
                                        />
                                    </Button> */}

                                    {/* Modal pour choisir la nouvelle colonne */}
                                    {/* <Modal
                                        visible={this.state.modalVisibleListColumns}
                                        transparent={true}
                                        onRequestClose={() => this.modalVisibleListColumns(!this.modalVisibleListColumns)}
                                    >
                                        <Block style={styles.modal}>
                                            <FlatList
                                                data={this.state.listColumns}
                                                renderItem={({ item, index }) => (
                                                    <Button index={index}
                                                        onPress={() => {
                                                            this.setState({
                                                                numColumn: item.id
                                                            });
                                                            this.modalVisibleListColumns(!this.modalVisibleListColumns)
                                                        }}
                                                    >
                                                        <Text size={fontsize.body} color={theme.COLORS.WHITE}>
                                                            {item.title}
                                                        </Text>
                                                    </Button>
                                                )}
                                            />
                                        </Block>
                                    </Modal> */}

                                    {/* Bouton Valider */}
                                    <Button shadowless onPress={() => this.updateTask()} style={styles.formbutton}>
                                        <Text size={fontsize.body} color={theme.COLORS.WHITE}>
                                            Modifier
                                        </Text>
                                    </Button>
                                </Block>
                            </Block>
                        </TouchableWithoutFeedback>
                    </KeyboardAvoidingView>
                </Block>
            // </ScrollView>
        );
    }

    // Fonction qui modifie une tâche
    updateTask = async () => {
        try {
            const { navigation, route } = this.props;
            const detailsColumn = route.params.item.columnId;
            const URL = `/tasks/update/${this.state.taskId}`;
            
            if (this.state.updatedTask.title !== null) {
                const task = new FormData();
                task.append('title', this.state.updatedTask.title);
                task.append('content', this.state.updatedTask.content);
                if (this.state.image === false && this.state.newImage === null) {
                    task.append('image', null);
                } else if (this.state.image === false && this.state.newImage !== null) {
                    task.append('image', { uri: this.state.newImage, type: 'image/*', name: this.state.nomImage });
                } else { }
                task.append('columnId', this.state.columnId);

                let response = await todolistApiService.request({
                    url: URL,
                    method: 'POST',
                    data: task,
                    headers: {
                        "Content-type": "multipart/form-data"
                    }
                });

                if (response.data.result) {
                    apiLogger.success('La tâche a bien été modifiée.');
                    navigation.navigate('TasksStack_Columns', { item: detailsColumn });
                }
            } else {
                Alert.alert(config.app.name, 'Vous devez renseigner un titre.');
                appLogger.error('Erreur lors de la modification de la tâche.');
            }
        } catch (error) {
            Alert.alert(config.app.name, 'Il y a eu une erreur lors de la modification de la tâche.');
            apiLogger.error('Erreur lors de la modification de la tâche.');
            apiLogger.error(error);
        }
    }
}