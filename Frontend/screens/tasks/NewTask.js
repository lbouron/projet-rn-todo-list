import React from 'react';
import {
    Dimensions,
    KeyboardAvoidingView,
    TouchableWithoutFeedback,
    Keyboard,
    TouchableOpacity,
    Image,
    Alert
} from 'react-native';
import { Block, Text } from 'galio-framework';
import * as ImagePicker from 'expo-image-picker';

import { Input, Button, Icon } from '../../components';
import { theme, fontsize } from '../../constants';
import { styles } from '../../assets/css/styles';

import todolistApiService from '../../services/todolistApiService';
import config from '../../config';
import { apiLogger, appLogger } from '../../services/logger';

const { width, height } = Dimensions.get('screen');

export default class NewTask extends React.Component {
    constructor(props) {
        super(props);

        this.navigation = props.navigation;

        this.state = {
            title: null,
            content: null,
            image: null,
            nomImage: '',
            loading: false
        };
    }

    // Fonction pour sélectionner une image dans la galerie du téléphone
    showImagePicker = async () => {
        // Demande les permissions pour ouvrir la galerie d'images
        const permissions = await ImagePicker.requestMediaLibraryPermissionsAsync();

        // Accès refusé
        if (permissions.granted === false) {
            alert('L\'accès à la galerie a été refusé.');
            return;
        }

        // Accès autorisé, ouvre la galerie d'images
        let picker = await ImagePicker.launchImageLibraryAsync({
            mediaTypes: ImagePicker.MediaTypeOptions.Images,
            // allowsEditing: true
        });

        // Envoi de l'image sélectionnée dans le state
        if (!picker.cancelled) {
            let nomUri = picker.uri.split('/').pop();
            this.setState({
                image: picker.uri,
                nomImage: nomUri
            });
        }
    }

    render() {
        return (
            <KeyboardAvoidingView behavior='position' style={styles.container}>
                <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
                    <Block center>
                        {/* Sélection de l'image dans la galerie */}
                        <TouchableOpacity onPress={() => this.showImagePicker()}>
                            <Block style={{ ...styles.image, alignItems: 'center', justifyContent: 'center' }}>
                                {this.state.image !== null ?
                                    (<Image
                                        source={{ uri: this.state.image }}
                                        style={styles.image}
                                        resizeMode='cover'
                                    />)
                                    :
                                    (<Icon
                                        name='plus-circle'
                                        family='font-awesome'
                                        size={40}
                                        color={theme.COLORS.PRIMARY}
                                    />)
                                }
                            </Block>
                        </TouchableOpacity>

                        {/* Inputs du titre et du contenu */}
                        <Block style={{ height: height / 2.5, justifyContent: 'space-between' }}>
                            <Block>
                                <Block row style={{ alignItems: 'center' }}>
                                    <Text
                                        size={fontsize.body}
                                        style={{
                                            position: 'absolute',
                                            zIndex: 2,
                                            marginLeft: 15
                                        }}
                                    >
                                        Titre
                                    </Text>

                                    <Input
                                        shadowless
                                        placeholder='Titre'
                                        placeholderTextColor={theme.COLORS.HEADER}
                                        style={{
                                            ...styles.input,
                                            borderTopLeftRadius: 10,
                                            borderTopRightRadius: 10
                                        }}
                                        onChangeText={title => this.setState({ title })}
                                    />
                                </Block>

                                <Block row style={{ marginTop: -18, alignItems: 'flex-start' }}>
                                    <Text
                                        size={fontsize.body}
                                        style={{
                                            position: 'absolute',
                                            marginLeft: 15,
                                            marginTop: 24,
                                            zIndex: 2
                                        }}
                                    >
                                        Description
                                    </Text>

                                    <Input
                                        shadowless
                                        placeholder='Description de la tâche'
                                        placeholderTextColor={theme.COLORS.HEADER}
                                        multiline={true}
                                        style={{
                                            ...styles.input,
                                            height: 190,
                                            paddingVertical: 10,
                                            alignItems: 'flex-start',
                                            borderBottomLeftRadius: 10,
                                            borderBottomRightRadius: 10
                                        }}
                                        onChangeText={content => this.setState({ content })}
                                    />
                                </Block>
                            </Block>

                            {/* Bouton Valider */}
                            <Button shadowless onPress={() => this.createTask()} style={styles.formbutton}>
                                <Text size={fontsize.body} color={theme.COLORS.WHITE}>
                                    Valider
                                </Text>
                            </Button>
                        </Block>
                    </Block>
                </TouchableWithoutFeedback>
            </KeyboardAvoidingView>
        );
    }

    // Fonction qui crée une tâche
    createTask = async () => {
        try {
            const { navigation, route } = this.props;
            const detailsColumn = route.params.item;
            const URL = `/tasks/add`;
            
            if (this.state.title !== null) {
                const task = new FormData();
                task.append('title', this.state.title);
                task.append('content', this.state.content);
                task.append('image', { uri: this.state.image, type: 'image/*', name: this.state.nomImage });
                task.append('columnId', detailsColumn);

                let response = await todolistApiService.request({
                    url: URL,
                    method: 'PUT',
                    data: task,
                    headers: {
                        "Content-type": "multipart/form-data"
                    }
                });

                if (response.data.result) {
                    apiLogger.success('La tâche a bien été créée.');
                    navigation.navigate('TasksStack_Columns', { item: detailsColumn });
                }
            } else {
                Alert.alert(config.app.name, 'Vous devez renseigner un titre.');
                appLogger.error('Erreur lors de la création de la tâche.');
            }
        } catch (error) {
            Alert.alert(config.app.name, 'Il y a eu une erreur lors de la création de la tâche.');
            apiLogger.error('Erreur lors de la création de la tâche.');
            apiLogger.error(error);
        }
    }
}