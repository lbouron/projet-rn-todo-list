import { logger, consoleTransport } from "react-native-logs";

const defaultConfig = {
    levels: {
        trace: 0,
        debug: 1,
        info: 2,
        success: 3,
        warn: 4,
        error: 5
    },
    severity: "debug",
    transport: consoleTransport,
    transportOptions: {
        colors: {
            info: "blueBright",
            success: "green",
            warn: "yellowBright",
            error: "redBright",
        },
        extensionColors: {
            api: "magenta",
            app: "blueBright",
        },
    },
    async: true,
    dateFormat: "time",
    printLevel: true,
    printDate: true,
    enabled: true,
};

const log = logger.createLogger(defaultConfig);

const apiLogger = log.extend("api");
const appLogger = log.extend("app");

export { apiLogger, appLogger };