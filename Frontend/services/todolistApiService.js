import axios from 'axios';
import config from '../config';

const apiService = axios.create({
    baseURL: config.serverApi.route,
    timeout: 5000,
    headers: {
        'Content-Type': 'application/json'
    }
});

export default apiService;