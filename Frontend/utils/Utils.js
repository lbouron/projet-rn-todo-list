import connectedProfile from '../authentication/ConnectedProfile';
import todolistApiService from '../services/todolistApiService';
import { apiLogger, appLogger } from '../services/logger';
import config from '../config';

async function getDashboardsByUser() {
    let data = [];

    try {
        let response = await todolistApiService.request({
            url: '/users/' + connectedProfile.id + '/dashboards',
            method: 'GET'
        });

        if (response.data.result !== null) {
            data = response.data.result;
        } else {
            throw new Error();
        }
    } catch (error) {
        data = null;
    }

    return data;
}

async function getColumnsByDashboard(dashboard_id) {
    let data = [];

    try {
        let response = await todolistApiService.request({
            url: '/dashboards/' + dashboard_id + '/columns',
            method: 'GET'
        });

        if (response.data.result !== null) {
            data = response.data.result;
        } else {
            throw new Error();
        }
    } catch (error) {
        data = null;
    }

    return data;
}

async function getColumnsByDashboardWithTasks(dashboard_id) {
    let data = [];

    try {
        let response = await todolistApiService.request({
            url: '/dashboards/' + dashboard_id + '/columnswithtasks',
            method: 'GET'
        });

        if (response.data.result !== null) {
            data = response.data.result;
        } else {
            throw new Error();
        }
    } catch (error) {
        data = null;
    }

    return data;
}

const Utils = {
    getDashboardsByUser,
    getColumnsByDashboard,
    getColumnsByDashboardWithTasks
}

export default Utils;